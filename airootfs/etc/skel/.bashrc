#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'

export PS1="\[\033[38;5;12m\][\[$(tput sgr0)\]\[\033[38;5;10m\]\u\[$(tput sgr0)\]\[\033[38;5;12m\]@\[$(tput sgr0)\]\[\033[38;5;7m\]\h\[$(tput sgr0)\]\[\033[38;5;12m\]]\[$(tput sgr0)\]\[\033[38;5;15m\]: \[$(tput sgr0)\]\[\033[38;5;7m\]\w\[$(tput sgr0)\]\[\033[38;5;12m\]>\[$(tput sgr0)\]\[\033[38;5;10m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

[ -e "/etc/DIR_COLORS" ] && DIR_COLORS="/etc/DIR_COLORS"
[ -e "$HOME/.dircolors" ] && DIR_COLORS="$HOME/.dircolors"
[ -e "$DIR_COLORS" ] || DIR_COLORS=""
eval "`dircolors -b $DIR_COLORS`"



### I ADDED THIS ###
export VISUAL="nano"

### Set alias
#############

alias ll="ls -la"
alias ls="ls --color=auto "
alias l="ls"
alias la="ls -a"
alias sf="screenfetch"
alias c="clear"
alias x="exit"
alias cls="clear"
alias off="shutdown -h now"
alias rut="sudo su"
alias root="sudo su"
alias leaf="leafpad"
alias snan="sudo nano"
alias sgny="sudo geany"
alias swifi="sudo wifi-menu"
alias ..="cd .."
alias ...="cd ../cd .."
alias rbt="reboot"
alias p5="ping -c5 google.com"
alias p3="ping -c3 google.com"
alias wf="wifi-menu"

######ARCH LINUX#######< 
alias pac="pacman"
alias pacs="pacman -S"
alias yrts="yaourt -S"
alias yrt="yaourt"
alias inst="pacman -S"
alias upd="pacman -Sy"
alias supd="sudo pacman -Sy"
alias sup="pacman -Syu"
alias ssup="sudo pacman -Syu"
alias upg="pacman -Syu"
alias supg="sudo pacman -Syu"
alias grb="grub-mkconfig -o /boot/grub/grub.cfg"
alias arm="pacman -R $(pacman -Qdtq)"

