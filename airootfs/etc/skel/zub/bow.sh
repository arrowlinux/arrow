#!/bin/bash

#======================================|
#              WELCOME TO              |
#======================================|
#  _____    ______    __    __    __   |
# |     \  /  __  \  |  |  |  |  |  |  | Better
# | **  / |  /  \  | |  |  |  |  |  |  | Or
# |    |  |  |  |  | |  |  |  |  |  |  | Worse
# | **  \ |  \__/  | |   \/    \/   |  |
# |_____/  \______/   \_____/\_____/   |
#======================================|
# Built for Arch Linux
#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 14/Jan/2019 undergoing development to date.
#------------------------------------------------------------------------------
# BOW is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Or visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch, it was brought to life by  
# an overwhelming number of requests by the arrowlinux group.
#
# For contributions, code, ideas, and other help,
# I give my deepest thanks to: knowledgebase101, erw1030, 
# leon.p (From EzeeTalk), and the arrowlinux group.
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       paypal.me/eliwal
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
#
#------------------------------------------------------------------------------
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if Zenity is installed. If not, it will install it.
TODAY="$(date)"

if [[ $USER = "root"  ]]
 then
        clear
      if [ -f /usr/bin/dialog ]
           then clear
                clear
                if [ -f /srv/dial.txt ]
                  then touch /srv/dial.txt && echo "This tells bow.sh that Dialog is already installed, and can be skipped." > /srv/dial.txt

                  else echo " Dialog is already installed"
                       touch /srv/dial.txt && echo "This tells bow.sh that Dialog is already installed, and can be skipped." > /srv/dial.txt
                       sleep 2
                fi
           else pacman -Sy --noconfirm dialog
                touch /srv/dial.txt && echo "This tells bow.sh that Dialog is already installed, and can be skipped." > /srv/dial.txt
                clear
                clear 
      fi
      if [ -f /usr/bin/glances ]
          then 
               if [ -f /srv/glanc.txt ]
                  then touch /srv/glanc.txt && echo "This tells bow.sh that Glances is already installed, and can be skipped." > /srv/glanc.txt

                  else echo " Glances is already installed"
                     touch /srv/glanc.txt && echo "This tells bow.sh that Glances is already installed, and can be skipped." > /srv/glanc.txt
                     sleep 2
               fi
          else pacman -Sy --noconfirm glances
               touch /srv/glanc.txt && echo "This tells bow.sh that Glances is already installed, and can be skipped." > /srv/glanc.txt
               clear ; clear 
      fi
      if [ -f /usr/bin/reflector ]
         then
              if [ -f /srv/reflect.txt ]
                 then touch /srv/reflect.txt && echo "This tells bow.sh that Reflector is already installed, and can be skipped." > /srv/reflect.txt

                 else
                     echo " Reflector is already installed"
                     echo ""
                     touch /srv/reflect.txt && echo "This tells bow.sh that Reflector is already installed, and can be skipped." > /srv/reflect.txt
                     sleep 2
              fi
         else 
              if [ -f /srv/reflect.txt ]
                  then 
                       touch /srv/reflect.txt
                            
                  else 
                      pacman -S --noconfirm reflector
              fi
              if [[ "$?" = "0" ]]
                 then
                     touch /srv/reflect.txt && echo "This tells bow.sh that Reflector is already installed, and can be skipped." > /srv/reflect.txt
                     clear
                     clear
                else  
                     clear
                     clear
                     dialog --title "${DWS1}" --no-lines --yesno "ERROR! \n\nReflector could not be installed. \nIf you are using a Manjaro distribution, \nReflector is not Available. \nManjaro uses Their own repositories. \nthe \"Refresh Mirrorlist\" option will not function. \n\nHowever everthing else should work fine. \n\n\nWould you like to see this message the next time you run BOW?" 20 45
                                if [[ "$?" = "1" ]]
                                   then 
                                         touch /srv/reflect.txt && echo "This tells bow.sh that Reflector is NOT installed, but that you want it to be skipped." > /srv/reflect.txt
                                  else echo ""
                                fi

             fi
      fi

  echo " ROOT check passed "
  echo ""
  echo " =================================================="
  echo " ========== $TODAY =========="
  else
         dialog --no-lines --msgbox "ROOT check FAILED!\n\nIn a terminal run this with sudo.\n\nlike this     sudo ./bow.sh\nOr this       sudo bash bow.sh" 10 40 #background-color: red
         dialog --no-lines --msgbox "BOW will now close ! ! " 6 30
         clear
         clear
       echo " ROOT check FAILED (must be ran with sudo) like this "
       echo ""
       echo " sudo ./bow.sh"
       echo ""
       echo " =================================================== "
        exit "${?}"
fi
##################################################################################
#=================================================================================
#=================================================================================
# if you have an issue with the variable below, (Line #98) then change it    #====
# Enter your User Name there.                                                #====
#   Like this: USR1='username'   or   USR1="username"                        #====
USR1="$(logname)"                                                            #====
#=================================================================================
#=================================================================================
##################################################################################
if [ -f /usr/bin/pacaur ]
   then
        AUR="pacaur"
   else
       if [ -f /usr/bin/yay ]
          then
              AUR="yay"
          else
              if [ -f /usr/bin/pakku ]
                 then
                     AUR="pakku"
                 else
                     if [ -f /usr/bin/trizen ]
                        then
                            AUR="trizen"
                        else
                            if [ -f /usr/bin/yaourt ]
                               then
                                   AUR="yaourt"
                            fi
                     fi
              fi
       fi
fi
       
# DWS1='BOW 2.0 Workshop'
DWS1='BOW Workshop'
title='BOW toolbox'
AUR1='Choose An AUR Helper'
AUR2='Add/Remove AUR Helper'

# AUR="$( cat /srv/AURhlpr.txt )"
ARAUR='Add/Remove AUR Manager'
#------------------------------------------------------------------------------
EXT1='EXIT Options'
EXT2='Exit BOW'
EXT3='Just Exit This App'
UPO1='Update Options'
UPA1='Easy Update '
UPA2="Easy Update +AUR With ${AUR^^}"
IAD='Install another Desktop'
PPA1='PPA Management'

SFM='Swapfile Management'
S0N="Turn All Swap On"
S0FF="Turn All Swap Off"

CHP1='USERS and Passwords'
GP1='Generate a Random Password'
LF1='Lotto Fun'
HTU='How to use this Toolbox'
HTU2='How to use this Tool'
BACK_HOME='Back to Home Menu' 
BTAS='Back to Arch Setup' 
#------------------------------------------------------------------------------
TTLA="${USR1^} Pick An Update Option"
URS1='Update Repository List'
LAUB1='List Available Updates'
LAUB2='List Available Updates +AUR'

AUS1='Apply Updates (Safe)'
AULS1='Apply Updates (Less Safe)'
UPG1='Update Bootloader (grub)'
#------------------------------------------------------------------------------
kubu1='Install KDE Desktop'
lubu1='Install Lxde Desktop'
xubu1='Install Xfce Desktop'
gnome1='install Gnome 3 Desktop'
MATE1='Install MATE Desktop'
I3_GAPS='Insall I3-Gaps'
OPB1='Install OPENBOX'
#------------------------------------------------------------------------------
MES1='Manually Enter Size'
DS1='Delete Swapfile'
LFS1='Look At fstab, This is Safe'
ASF1='Add swapfile to fstab,  Dangerous! '
#------------------------------------------------------------------------------
USR="Change Password For ${USR1^} "
RT1='Change Password For ROOT'
OTR1='Change Password For Other User'
WH0='List ALL USERS on this Computer'
ANU1='Add a NEW User to the Computer'
DEL1='DELETE / REMOVE User (Danger!)'
RML1='Refresh Mirrorlist'
CHHN='Change Hostname'
HowTU3='How to use This Tool'
#------------------------------------------------------------------------------
PA1='Power Adminastration'
RB1='Reboot This Computer'
SD1='Shut Down This Computer'
#------------------------------------------------------------------------------
HOM3='Back To Home Menu'
hstnm="$(hostname)"
SSP='Suspend to RAM. Sleep'
HBRNT='Hibernate. Power Off'
LOUT='Log Out Of Desktop'
ASUI="Arch Setup/Install"
ILIM="Install Login Manager"

while true ; do



# home, update
HOME_HEIGHT=15
HOME_WIDTH=33
HOME_CHOICE_HEIGHT=8

UPDATE_HEIGHT=15
UPDATE_WIDTH=40
UPDATE_CHOICE_HEIGHT=8

IAD_HEIGHT=17
IAD_WIDTH=35
IAD_CHOICE_HEIGHT=12

PPA_HEIGHT=26
PPA_WIDTH=40
PPA_CHOICE_HEIGHT=24

PPA_HEIGHT2=16
PPA_WIDTH2=40
PPA_CHOICE_HEIGHT2=9

SWAP_HEIGHT=25
SWAP_WIDTH=45
SWAP_CHOICE_HEIGHT=25

USER_HEIGHT=18
USER_WIDTH=42
USER_CHOICE_HEIGHT=15

LOTTO_HEIGHT=20
LOTTO_WIDTH=40
LOTTO_CHOICE_HEIGHT=10

HTU_HEIGHT=20
HTU_WIDTH=40
HTU_CHOICE_HEIGHT=10

NEW_HEIGHT=20
NEW_WIDTH=40
NEW_CHOICE_HEIGHT=10

ASUI_HEIGHT=15
ASUI_WIDTH=35
ASUI_CHOICE_HEIGHT=15

ILIM_HEIGHT=15
ILIM_WIDTH=35
ILIM_CHOICE_HEIGHT=15

EXIT_HEIGHT=12
EXIT_WIDTH=35
EXIT_CHOICE_HEIGHT=6

AURHLP_HEIGHT=12
AURHLP_WIDTH=35
AURHLP_CHOICE_HEIGHT=8

AUR_HEIGHT=12
AUR_WIDTH=32
AUR_CHOICE_HEIGHT=8

AUR3_HEIGHT=15
AUR3_WIDTH=32
AUR3_CHOICE_HEIGHT=8

AURAR_HEIGHT=15
AURAR_WIDTH=40
AURAR_CHOICE_HEIGHT=8

HEIGHT=20
HEIGHT2=10
HEIGHT3=22

WIDTH=40
WIDTH2=50

CHOICE_HEIGHT2=12
CHOICE_HEIGHT3=22
CHOICE_HEIGHT4=20
CHOICE_HEIGHT=10
CHOICE_HEIGHT7=3
MENU="Choose an option:"
MMNU="Main Menu"

OPTIONS=(1 "$UPO1"
         2 "$SFM"
         3 "$CHP1"
         4 "$LF1"
         5 "$ASUI"
         6 "$HTU" 
         7 "$EXT1")

CHOICE=$(dialog --clear \
                --ok-label "continue" \
                --no-cancel \
                --no-lines \
                --backtitle "$DWS1" \
                --title "$MMNU" \
                --menu "$MENU" \
                $HOME_HEIGHT $HOME_WIDTH $HOME_CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
###############################################################################
########################### Update Management #################################
###############################################################################

        1)
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$LAUB1"
                          2 "$LAUB2"
                          3 "$UPA1"
                          4 "$UPA2"
                          5 "$ARAUR"
                          6 "$UPG1" 
                          7 "$EXT2"
                          8 "$HOM3")

                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$UPO1" \
                                 --menu "$MENU" \
                                 $UPDATE_HEIGHT $UPDATE_WIDTH $UPDATE_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in

# list updates with pacman
                         1) 
                            clear; clear ; pacman -Sy --color always ; clear ; clear ; OUTP="$(sudo pacman -Qu | wc -l)" ; pacman -Qu --color always ; 
                            echo ""
                            echo -e "\e[36mAvailable Repository Updates:\e[93m ${OUTP} \e[36m(Not Including AUR) \e[0m "
                            echo ""
                            echo -e "Hello ${USR1^} This Was Ran With \e[92mPacman\e[92m\e[0m"
                            echo ""
                            echo "Press a key to continue"
                            read -rsn1
                            echo "Key pressed" ; sleep 1
                             ;;

# list updates with AUR Manager
                         2)
                            
                                            if [ -f /usr/bin/pacaur ]
                                               then   
                                                   clear ; clear
                                                   echo ""
                                                   echo "Hello ${USR1^} you are using Pacaur"
                                                   echo ""
                                                   sudo -u $USR1 pacaur -Sy ; clear ; clear ; OUTP2="$(sudo -u $USR1 pacaur -Qu | wc -l)" ; sudo -u $USR1 pacaur -Qu --color always ; echo "" ; echo "${OUTP2} Available Updates"
                                                   echo ""
                                                   echo -e "Hello ${USR1^} This Was Ran With \e[92mPacaur\e[92m\e[0m"
                                                   echo ""
                                                   echo "Press a key to continue"
                                                   read -rsn1
                                                   echo "Key pressed" ; sleep 1
                                               else
                                                  if [ -f /usr/bin/yay ]
                                                    then   
                                                      clear ; clear
                                                      sudo -u $USR1 yay -Sy
                                                      clear ; clear
                                                      OUTP6="$(sudo -u $USR1 yay -Qu --repo | wc -l)"
                                                      echo -e "\e[31mUpdates From The Repository\e[0m"
                                                      sudo -u $USR1 yay -Qu --color always --repo
                                                      echo ""

                                                      OUTP7="$(sudo -u $USR1 yay -Qau | wc -l)"
                                                      echo -e "\e[36mAvailable Repository Updates:\e[93m ${OUTP6}\e[0m"
                                                      echo ""
                                                      echo -e "\e[31mUpdates From AUR\e[0m"
                                                      sudo -u $USR1 yay -Qau --color always
                                                      echo ""
                                                      echo -e "\e[36mAvailable AUR Updates:\e[93m ${OUTP7}\e[0m"
                                                      echo ""
                                                      echo -e "Hello ${USR1^} This Was Ran With \e[92mYay\e[92m\e[0m"
                                                      echo ""
                                                      echo "Press a key to continue"
                                                      read -rsn1
                                                      echo "Key pressed" ; sleep 1
                                                    else
                                                       if [ -f /usr/bin/pakku ]
                                                         then   
                                                           clear ; clear
                                                           echo ""

                                                           echo "Hello ${USR1^} you are using Pakku"
                                                           echo ""
                                                           sudo -u $USR1 pakku -Sy --color always ; clear ; clear ; OUTP2="$(sudo -u $USR1 pakku -Qu | wc -l)" ; sudo -u $USR1 pakku -Qu --color always ; echo "" ; echo "${OUTP2} Available Updates"
                                                           echo ""
                                                           echo "Hello ${USR1^} This Was Ran With Pakku"
                                                           echo ""
                                                           echo "Press a key to continue"
                                                           read -rsn1
                                                           echo "Key pressed" ; sleep 1
                                                         else
                                                           if [ -f /usr/bin/trizen ]
                                                             then   
                                                                clear ; clear
                                           #                     echo ""
                                                              sudo -u $USR1 trizen -Sy
                                                              clear ; clear
                                                                OUTP2="$(sudo -u $USR1 trizen -Qru | wc -l)"
                                                              echo -e "\e[31mUpdates From The Repository\e[0m"
                                                              sudo -u $USR1 trizen -Qru --color always
                                                              echo ""
                                                              OUTP3="$(sudo -u $USR1 trizen -Qau | wc -l)"
                                                              echo ""
                                                              echo -e "\e[36mAvailable Repository Updates:\e[93m ${OUTP2}\e[0m"
                                                              echo ""
                                                              echo -e "\e[31mUpdates From AUR\e[0m"
                                                              sudo -u $USR1 trizen -Qau --color always
                                                              echo ""
                                                              echo -e "\e[36mAvailable AUR Updates:\e[93m ${OUTP3}\e[0m"
                                                                echo ""
                                                                echo -e "Hello ${USR1^} This Was Ran With \e[92mTrizen\e[92m\e[0m"
                                                                echo ""
                                                                echo "Press a key to continue"
                                                                read -rsn1
                                                                echo "Key pressed" ; sleep 1
                                                             else
                                                               if [ -f /usr/bin/yaourt ]
                                                                 then   
                                                                    clear ; clear
                                                                    echo ""
                                                                    
                                                                   echo "Hello ${USR1^} you are using Yaourt"
                                                                   echo ""
                                                                   yaourt -Sy --color ; clear ; clear ; OUTP2="$(sudo -u $USR1 yaourt -Qau | wc -l)" ;  yaourt -Qau --color ; echo "" ; echo "${OUTP2} Available Updates"
                                                                   echo ""
                                                                   echo "Hello ${USR1^} This Was Ran With YAOURT"
                                                                   echo ""
                                                                   echo "Press a key to continue"
                                                                   read -rsn1
                                                                   echo "Key pressed" ; sleep 1
                                                                 else
                                                                    echo "An error has occurred!"
                                                                    echo "Please make sure an AUR helper is installed "
                                                                    echo "And try again."
                                                                    echo ""
                                                                    echo "Press a key to continue"
                                                                    read -rsn1
                                                                    echo "Key pressed" ; sleep 1


                      # Installl an AUR Managr
    
                                         OPTIONS=(
                                                  1 "Install pacaur"
                                                  2 "Install yay"
                                                  3 "Install pakku"
                                                  4 "Install trizen" 
                                                  5 "Install Yaourt"
                                                  6 "Exit Bow")
                        
                                         CHOICE=$(dialog --clear \
                                                         --ok-label "Continue" \
                                                         --cancel-label "Back" \
                                                         --no-lines \
                                                         --backtitle "$DWS2" \
                                                         --title "$AUR1" \
                                                         --menu "$MENU9" \
                                                         $AUR_HEIGHT $AUR_WIDTH $AUR_CHOICE_HEIGHT \
                                                         "${OPTIONS[@]}" \
                                                         2>&1 >/dev/tty)
                                         case $CHOICE in
                      
                                              # Install pacaur
                                                         3)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           pacman -S git --noconfirm

                                                           cd /home/$USR1/
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/auracle-git.git
                                                           cd /home/$USR1/auracle-git/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/auracle-git

                                                           sudo -u $USR1 git clone https://aur.archlinux.org/pacaur.git
                                                           cd /home/$USR1/pacaur/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/pacaur

                                                           echo ""
                                                           echo "Pacaur is installed."
                                                           echo ""
                                                           echo "Press a key to continue"
                                                           read -rsn1
                                                           echo "Key pressed" ; sleep 1
                                                           ;;

                                              # Install yay

                                                         2)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/yay.git
                                                           cd yay
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -dR yay/
                                                           ;;

                                           # Install pakku
                                                         3)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/pakku.git
                                                           cd pakku
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -dR /home/$USR1/pakku/
                                                           ;;
                                            # Install trizen
                                       
                                                          4) 
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/trizen.git
                                                           cd trizen
                                                           sudo -u $USR1 makepkg -si #--noconfirm
                                                           cd ..
                                                           rm -dR /home/$USR1/trizen/ 
                                                           ;;
                                          # Install yaourt

                                                         5)
                                                          clear ; clear
                                                          echo "user name is $USR1"
                                                          cd /home/"${USR1}"
                                                          pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                                          sudo -u $USR1 git clone https://aur.archlinux.org/package-query.git
                                                          cd /home/$USR1/package-query/ && sudo -u $USR1 makepkg -si --noconfirm
                                                          cd ..
                                                          sudo -u $USR1 git clone https://aur.archlinux.org/yaourt.git
                                                          cd /home/$USR1/yaourt/ && sudo -u $USR1 makepkg -si --noconfirm
                                                          cd ..
                                                          rm -dR /home/$USR/yaourt package-query/

                                                          ;;

# Exit BOW
                                                         6)
                                                          clear ; clear
                                                          exit
                                                          ;;
                                         esac
    
                                                          fi
                                                        fi
                                                      fi
                                                    fi
                                                  fi                         

                             ;;
 # Update system with pacman

                         3) 

                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System\nWould you like to continue?" 6 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else
                                              clear; clear ; pacman -Sy --color always ; clear ; clear ; OUTP="$(sudo pacman -Qu | wc -l)" ; pacman -Qu --color always ; echo "" ; echo -e "\e[36mAvailable Repository Updates:\e[93m ${OUTP} \e[36m(Not Including AUR) \e[0m "
                                              echo ""
                                              echo -e "Hello ${USR1^} This Was Ran With \e[92mPacman\e[92m\e[0m" 
                                              echo "" ; sleep 10
                                              
                                              pacman -Syu --noconfirm  --color always && clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                fi
                             ;; 

 # Update system with AUR Manager

                         4)
                                   
                                        
                                            if [ -f /usr/bin/pacaur ]
                                               then   
                                                   dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System With Pacaur \nWould you like to continue?" 6 45
                                                   if [[ "$?" = "1" ]]
                                                      then
                                                          dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ; clear ; clear break 1
                                                      else
                                                   clear ; clear

                                                   echo ""
                                           
                                                   echo "Hello ${USR1^} you are using Pacaur"
                                                   echo ""
                                                   sudo -u $USR1 pacaur -Sy --color always ; clear ; clear ; OUTP2="$(sudo -u $USR1 pacaur -Qu | wc -l)" ; sudo -u $USR1 pacaur -Qu --color always ; echo "" ; echo "${OUTP2} Available Updates"
                                                   echo ""
                                                   echo "Hello ${USR1^} This Was Ran With Pacaur" ; sleep 10

                                                      sudo -u ${USR1} pacaur -Su --noconfirm  --color always && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                                   
                                              fi
                                            else
                                                if [ -f /usr/bin/yay ]
                                                  then 
                                                      dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System With Yay \nWould you like to continue?" 6 45
                                                   if [[ "$?" = "1" ]]
                                                      then
                                                          dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ; clearr ; clear break 1
                                                      else      
                                                      clear ; clear
                          #                            echo ""

                          #                            echo "Hello ${USR1^} This Was Ran With Yay"
                          #                            echo ""
                                                      sudo -u $USR1 yay -Sy color aways
                                                      clear ; clear
                                                      OUTP6="$(sudo -u $USR1 yay -Qu --repo | wc -l)"
                                                      echo -e "\e[31mUpdates From The Repository\e[0m"
                                                      sudo -u $USR1 yay -Qu --color always --repo
                                                      echo ""

                                                      OUTP7="$(sudo -u $USR1 yay -Qau | wc -l)"
                                                      echo -e "\e[36mAvailable Repository Updates:\e[93m ${OUTP6}\e[0m"
                                                      echo ""
                                                      echo -e "\e[31mUpdates From AUR\e[0m"
                                                      sudo -u $USR1 yay -Qau --color always
                                                      echo ""
                                                      echo -e "\e[36mAvailable AUR Updates:\e[93m ${OUTP7}\e[0m"
                                                      echo ""
                                                      echo -e "Hello ${USR1^} This Was Ran With \e[92mYay\e[92m\e[0m" ; sleep 10
                                                      echo ""
                                                      sudo -u ${USR1} yay -Su --noconfirm  --color always && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                                   
                                                fi
                                               else
                                                   if [ -f /usr/bin/pakku ]
                                                      then  
                                                          dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System With Pakku \nWould you like to continue?" 6 45
                                                   if [[ "$?" = "1" ]]
                                                      then
                                                          dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ; clearr ; clear break 1
                                                      else   
                                                          clear ; clear
                                                          echo ""

                                                          echo "Hello ${USR1^} you are using Pakku"
                                                          echo ""
                                                          sudo -u $USR1 pakku -Sy --color always ; clear ; clear ; OUTP2="$(sudo -u $USR1 pakku -Qu | wc -l)" ; sudo -u $USR1 pakku -Qu --color always ; echo "" ; echo "${OUTP2} Available Updates"
                                                          echo ""
                                                          echo "Hello ${USR1^} This Was Ran With Pakku" ; sleep 10

                                                      sudo -u ${USR1} pakku -Su --noconfirm  --color always && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                                   
                                                     fi
                                                   else
                                                       if [ -f /usr/bin/trizen ]
                                                          then   
                                                              dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System With Trizen \nWould you like to continue?" 6 45
                                                   if [[ "$?" = "1" ]]
                                                      then
                                                          dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ; clearr ; clear break 1
                                                      else  
                                                              clear ; clear
                                                              sudo -u $USR1 trizen -Sy
                                                              clear ; clear
                                                              OUTP2="$(sudo -u $USR1 trizen -Qru | wc -l)"
                                                              echo -e "\e[31mUpdates From The Repository\e[0m"
                                                              sudo -u $USR1 trizen -Qru --color always
                                                              echo ""
                                                              OUTP3="$(sudo -u $USR1 trizen -Qau | wc -l)"
                                                              echo ""
                                                              echo -e "\e[36mAvailable Repository Updates:\e[0m ${OUTP2}"
                                                              echo ""
                                                              echo -e "\e[31mUpdates From AUR\e[0m"
                                                              sudo -u $USR1 trizen -Qau --color always
                                                              echo ""
                                                              echo -e "\e[36mAvailable AUR Updates:\e[0m ${OUTP3}"
                                                                echo ""
                                                                echo -e "Hello ${USR1^} This Was Ran With \e[92mTrizen\e[92m\e[0m" ; sleep 10

                                                               sudo -u ${USR1} trizen -Su --noconfirm  && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                                   
                                                            fi
                                                         else
                                                             if [ -f /usr/bin/yaourt ]
                                                                then 
                                                                    dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System With Yaourt \nWould you like to continue?" 6 45
                                                   if [[ "$?" = "1" ]]
                                                      then
                                                          dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ; clearr ; clear ; break 1
                                                      else    
                                                                   clear ; clear
                                                                   echo ""

                                                                   echo "Hello ${USR1^} you are using Yaourt"
                                                                   echo ""
                                                                   yaourt -Sy --color ; clear ; clear ; OUTP2="$(sudo -u $USR1 yaourt -Qau | wc -l)" ; yaourt -Qau --color ; echo "" ; echo "${OUTP2} Available Updates"
                                                                     echo ""
                                                                     echo "Hello ${USR1^} This Was Ran With YAOURT" ; sleep 10

                                                                   sudo -u ${USR1} yaourt -Sau --noconfirm  --color && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                                   

                                                          fi
                                                            else 
                  
                                                       OPTIONS=(
                                                                1 "Install Pacaur"
                                                                2 "Install Yay"
                                                                3 "Install Pakku"
                                                                4 "Install Trizen" 
                                                                5 "Install Yaourt"
                                                                6 "Exit Bow")
                                      
                                                       CHOICE=$(dialog --clear \
                                                                       --ok-label "Continue" \
                                                                       --cancel-label "Back" \
                                                                       --no-lines \
                                                                       --backtitle "$DWS2" \
                                                                       --title "$AUR1" \
                                                                       --menu "$MENU9" \
                                                                       $AUR_HEIGHT $AUR_WIDTH $AUR_CHOICE_HEIGHT \
                                                                       "${OPTIONS[@]}" \
                                                                       2>&1 >/dev/tty)
                                                       case $CHOICE in
                                    
                                            # Install pacaur

                                                         1)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           pacman -S git --noconfirm

                                                           cd /home/$USR1/
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/auracle-git.git
                                                           cd /home/$USR1/auracle-git/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/auracle-git

                                                           sudo -u $USR1 git clone https://aur.archlinux.org/pacaur.git
                                                           cd /home/$USR1/pacaur/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/pacaur

                                                           echo ""
                                                           echo "Pacaur is installed."
                                                           echo ""
                                                           echo "Press a key to continue"
                                                           read -rsn1
                                                           echo "Key pressed" ; sleep 1
                                                           ;;
                                            # Install yay

                                                         2)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/yay.git
                                                           cd yay
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -dR yay/
                                                           ;;

                                           # Install pakku
                                                         3)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/pakku.git
                                                           cd pakku
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -dR /home/$USR1/pakku/
                                                           ;;
                                           # Install trizen
                                       
                                                          4) 
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           cd /home/$USR1
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/trizen.git
                                                           cd trizen
                                                           sudo -u $USR1 makepkg -si #--noconfirm
                                                           cd ..
                                                           rm -dR /home/$USR1/trizen/ 
                                                           ;;
                                         # Install yaourt

                                                         5)
                                                          clear ; clear
                                                          echo "user name is $USR1"
                                                          cd /home/"${USR1}"
                                                          pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                                          sudo -u $USR1 git clone https://aur.archlinux.org/package-query.git
                                                          cd /home/$USR1/package-query/ && sudo -u $USR1 makepkg -si --noconfirm
                                                          cd ..
                                                          sudo -u $USR1 git clone https://aur.archlinux.org/yaourt.git
                                                          cd /home/$USR1/yaourt/ && sudo -u $USR1 makepkg -si --noconfirm
                                                          cd ..
                                                          rm -dR /home/$USR/yaourt/ package-query/

                                                          ;;

                                              # Exit BOW
                                                         6)
                                                          clear ; clear
                                                          exit
                                                          ;;
                                         esac
    
                            fi
                                                          fi
                                                        fi
                                                      fi
                                                    fi
                            ;;
# AUR Management
                         5) 



                          clear
                          clear  
                          while [[ "$?" = "0" ]] ; do
                          OPTIONS=(1 "Add An AUR Manager"
                                   2 "Remove An AUR Manager"
                                   3 "List Installed AUR Manager(s)"
                               #    4 "Reset AURhlpr.txt"
                                   4 "EXIT BOW"
                                   5 "$HOM3")
                 
                          CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$AUR2" \
                                 --menu "$MENU" \
                                 $AURAR_HEIGHT $AURAR_WIDTH $AURAR_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                          case $CHOICE in

                                   1)
                                         OPTIONS=(
                                                  1 "Install Pacaur"
                                                  2 "Install Yay"
                                                  3 "Install Pakku"
                                                  4 "Install Trizen" 
                                                  5 "Install Yaourt"
                                                  6 "Exit Bow"
                                                  7 "$HOM3")
                        
                                         CHOICE=$(dialog --clear \
                                                         --ok-label "Continue" \
                                                         --cancel-label "Back" \
                                                         --no-lines \
                                                         --backtitle "$DWS2" \
                                                         --title "$AUR2" \
                                                         --menu "$MENU9" \
                                                         $AUR3_HEIGHT $AUR3_WIDTH $AUR3_CHOICE_HEIGHT \
                                                         "${OPTIONS[@]}" \
                                                         2>&1 >/dev/tty)
                                         case $CHOICE in
                      
# Install pacaur

                                                1)
                                                           clear ; clear
                                                           echo "user name is $USR1"
                                                           pacman -S git --noconfirm

                                                           cd /home/$USR1/
                                                           sudo -u $USR1 git clone https://aur.archlinux.org/auracle-git.git
                                                           cd /home/$USR1/auracle-git/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/auracle-git

                                                           sudo -u $USR1 git clone https://aur.archlinux.org/pacaur.git
                                                           cd /home/$USR1/pacaur/
                                                           sudo -u $USR1 makepkg -si --noconfirm
                                                           cd ..
                                                           rm -R /home/$USR1/pacaur
                                                           echo ""
                                                           echo "Pacaur is installed."
                                                           echo ""
                                                           echo "Press a key to continue"
                                                           read -rsn1
                                                           echo "Key pressed" ; sleep 1
                                                           ;;

# Install yay

                                                2)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/yay.git
                                                  cd yay
                                                  sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -dR yay/
                                                  ;;

# Install pakku
                                                3)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/pakku.git
                                                  cd pakku
                                                  sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -dR /home/$USR1/pakku/
                                                  ;;
# Install trizen
                                       
                                                4) 
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/$USR1
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/trizen.git
                                                  cd trizen
                                                  sudo -u $USR1 makepkg -si #--noconfirm
                                                  cd ..
                                                  rm -dR /home/$USR1/trizen/ 
                                                  ;;
# Install yaourt

                                                5)
                                                  clear ; clear
                                                  echo "user name is $USR1"
                                                  cd /home/"${USR1}"
                                                  pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/package-query.git
                                                  cd /home/$USR1/package-query/ && sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  sudo -u $USR1 git clone https://aur.archlinux.org/yaourt.git
                                                  cd /home/$USR1/yaourt/ && sudo -u $USR1 makepkg -si --noconfirm
                                                  cd ..
                                                  rm -R /home/$USR/yaourt package-query/
              #                                    touch /srv/AURhlpr.txt && echo "yaourt" > /srv/AURhlpr.txt
                                                  ;;

                                              6)
                                                break 3
                                                ;;

                                              7)
                                                break 2
                                                ;;
                                          esac
                            ;;

                                   2)
                                      clear ; clear

                                         OPTIONS=(
                                                  1 "Remove Pacaur"
                                                  2 "Remove Yay"
                                                  3 "Remove Pakku"
                                                  4 "Remove Trizen" 
                                                  5 "Remove Yaourt"
                                                  6 "Remove ALL"
                                                  7 "Exit Bow"
                                                  8 "$HOM3")
                        
                                         CHOICE=$(dialog --clear \
                                                         --ok-label "Continue" \
                                                         --cancel-label "Back" \
                                                         --no-lines \
                                                         --backtitle "$DWS2" \
                                                         --title "$AUR2" \
                                                         --menu "$MENU" \
                                                         $AUR3_HEIGHT $AUR3_WIDTH $AUR3_CHOICE_HEIGHT \
                                                         "${OPTIONS[@]}" \
                                                         2>&1 >/dev/tty)
                                         case $CHOICE in
                                          1) clear ; clear
                                              if [ -f /usr/bin/pacaur ]
                                                 then
                                                 sudo pacman -Rs pacaur
                                              fi 
                                                 echo ""
                                                 echo "Pacaur has been Removed"
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            2) clear ; clear
                                              if [ -f /usr/bin/yay ]
                                                 then
                                                 sudo pacman -Rs yay
                                              fi 
                                                 echo ""
                                                 echo "yay has been Removed"
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            3) clear ; clear
                                              if [ -f /usr/bin/pakku ]
                                                 then
                                                 sudo pacman -Rs pakku
                                              fi
                                                 echo ""
                                                 echo "pakku has been Removed"
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;

                                            4) clear ; clear
                                              if [ -f /usr/bin/trizen ]
                                                 then
                                                 sudo pacman -Rs trizen
                                              fi
                                                 echo ""
                                                 echo "trizen has been Removed"
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;
                                            
                                            5) clear ; clear
                                              if [ -f /usr/bin/yaourt ]
                                                 then
                                                 sudo pacman -Rs yaourt
                                              fi
                                                 echo ""
                                                 echo "yaourt has been Removed"
                                                 echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              
                                              ;;
# Delete all AUR Helpers
                                            6) clear ; clear
                                              if [ -f /usr/bin/pacaur ]
                                                 then
                                                 sudo pacman -Rs pacaur
                                              fi

                                              if [ -f /usr/bin/yaourt ]
                                                 then
                                                 sudo pacman -Rs yaourt
                                              fi

                                              if [ -f /usr/bin/yay ]
                                                 then
                                                 sudo pacman -Rs yay
                                              fi

                                              if [ -f /usr/bin/pakku ]
                                                 then
                                                 sudo pacman -Rs pakku
                                              fi

                                              if [ -f /usr/bin/trizen ]
                                                 then
                                                 sudo pacman -Rs trizen
                                              fi

                                              echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              ;;

                                            7) clear ; clear ; break 3
                                              ;;

                                            8) clear ; clear ; break 2
                                              ;;  
                                        esac
                            ;;
# list installed AUR Managers
                                   3)
                                     clear ; clear
                                     if [ -f /usr/bin/yaourt ]
                                            then
                                                echo "yaourt is installed"
                                                echo ""

                                         fi

                                         if [ -f /usr/bin/pacaur ]
                                            then
                                                echo "Pacaur is installed"
                                                echo ""

                                         fi

                                         if [ -f /usr/bin/yay ]
                                            then
                                                echo "Yay is installed"
                                                echo ""

                                         fi

                                         if [ -f /usr/bin/pakku ]
                                            then
                                                echo "Pakku is installed"
                                                echo ""

                                         fi

                                         if [ -f /usr/bin/trizen ]
                                            then
                                                echo "Trizen is installed"
                                                echo ""

                                         fi
                                     echo "End of list"
                                     echo ""
                                     echo "Press a key to continue"
                                     read -rsn1
                                     echo "Key pressed" ; sleep 1

                                     ;;

                                # 5)
                                   4)
                                     clear ; clear ; break 3
                                     ;;

                                #  6)
                                   5)
                                     clear ; clear ; break 2
                                     ;;
                 esac
                 done           

                           ;;

                         6)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update GRUB Usually Safe\nBut Sometimes Not, Would you like to continue?" 7 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else clear ;echo "" ; echo "Updating GRUB Bootloader" ; echo ""
                                        grub-mkconfig -o /boot/grub/grub.cfg
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "GRUB Update Complete" 6 25
                                fi
                             ;; 

                         7)
                             clear && clear && break 2
                             ;;

                         8)
                             clear && clear && break 1
                             ;;
                 esac
                 done
            ;;


###############################################################################
############################## Swapfile Management ############################
###############################################################################

        2)
        
                 clear
                 clear  
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "choose size"
                          2 "2GB"
                          3 "4GB"
                          4 "6GB" 
                          5 "8GB"
                          6 "16GB"
                          7 "32GB"
                          8 "$S0N"
                          9 "$S0FF"
                          10 "Delete Swapfile"
                          11 "Look at fstab (Safe)"
                          12 "Let us add swapfile to fstab"
                          13 "Manually Edit fstab DANGEROUS!"
                          14 "Check RAM size with glances (SAFE) "
                          15 "EXIT BOW"
                          16 "$HOM3")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$SFM" \
                                 --menu "$MENU" \
                                 $SWAP_HEIGHT $SWAP_WIDTH $SWAP_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             mkfifo /tmp/namedPipe1 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "This is where you enter the \nsize of swapfile (in GB) you want\nNumbers only, like 4 or 8.5 or 16" 8 40 "4.5" 2> /tmp/namedPipe1 & 

                             # release contents of pipe
                             swF="$( cat /tmp/namedPipe1  )" 
                             clear ; clear
                             swapoff -a && rm -f /swapfile &&  fallocate -l ${swF}G /swapfile &&  chmod 600 /swapfile && mkswap /swapfile && swapon -a
                             if [[ "$?" = "1" ]]
                                then
                                    echo ""
                                    echo "Press a key to continue"
                                    read -rsn1
                                    echo "Key pressed" ; sleep 1
                                else 
                                    dialog --title "${DWS1}" --no-lines --msgbox "${swF}GB Swapfile Has Been Added" 8 30
                             fi
                             # clean up
                             rm /tmp/namedPipe1 
                             ;; 
#----------------------------------------------------------------------

                         2) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 2G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "2GB Swapfile Has Been Added" 8 30
                             ;;

                         3) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 4G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "4GB Swapfile Has Been Added" 8 30
                             ;;

                         4) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 6G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "6GB Swapfile Has Been Added" 8 30
                             ;;

                         5) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 8G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "8GB Swapfile Has Been Added" 8 30
                             ;;



                         6) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 16G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "16GB Swapfile Has Been Added" 8 30
                             ;;



                         7)
                             clear ; clear
                             swapoff -a
                             fallocate -l 32G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "32GB Swapfile Has Been Added" 8 30
                             ;;

                         8)
                            clear
                            clear
                            swapon -a
                            dialog --title "${S0N}" --no-lines --msgbox "All Swap Has Been Turned On" 6 33
                            ;;
                         


                         9)
                            clear
                            clear
                            swapoff -a
                            dialog --title "${S0N}" --no-lines --msgbox "All Swap Has Been Turned Off" 6 33
                  
                            ;;



                         10)
                             clear ; clear
                             swapoff -a && rm -f /swapfile
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;

                         11)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --msgbox "At the bottom of the following \nlist, you are looking for \n\n /swapfile none swap defaults 0 0" 10 50
                             echo ""
                             echo " Add this to the bottom of fstab."
                             echo " /swapfile none swap defaults 0 0"
                             echo ""
                             clear ; clear
                             cat /etc/fstab
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;
                         12)
                             echo "/swapfile none swap defaults 0 0" >> /etc/fstab
#                             nano /etc/fstab
                             dialog --title "${DWS1}" --no-lines --msgbox "swapfile was added to fstab. check to make sure it is, Make sure there is only one \ninstance of it.(no duplicates)" 10 50
                             ;;

                         13)
                             dialog --title "${DWS1}" --timeout 5 --no-lines --yesno  "Warning Editing fstab can \nBREAK YOUR SYSTEM! \n Are you sure you want to proceed?" 10 50
                             nano /etc/fstab
                             ;;

                         14)
                             
                             dialog --title "${DWS1}" --no-lines --msgbox "Glances will now open. \nIn Glances, \nRAM is identified as MEM \nWhen finished, \nEnter the letter Q or Esc." 10 35 ; clear

                                  glances ; clear
                             
                              ;;

                         15)
                             clear ; clear
                             break 2
                             ;;
                         16)
                             clear ; clear
                             break 1
                             ;;
                 esac
                 done

        ;;

###############################################################################
############################ USERS AND PASSWORDS ##############################
###############################################################################
        3)

                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$RT1"
                          2 "$USR"
                          3 "$OTR1"
                          4 "$GP1" 
                          5 "$WH0"
                          6 "$ANU1"
                          7 "$DEL1"
                          8 "$CHHN"
                          9 "EXIT BOW"
                          10 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$CHP1" \
                                 --menu "$MENU" \
                                 $USER_HEIGHT $USER_WIDTH $USER_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
#--------------------------------------------
# Change ROOT password
                         1) 
                             clear ; clear
                             passwd
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ROOT " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ROOT " 8 40
                                fi

                             ;; 
#--------------------------------------------
# Change USER password
                         2) 
                             clear ; clear
                             passwd $USR1
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${USR1^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${USR1^} " 8 40
                                fi
   
                             ;;
#-----------------------------------------------------------------------
# Change other users passwors 
                         3) 
                             clear ; clear
                             mkfifo /tmp/namedPipe2 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "enter new user name" 6 30 "${USR1,,}" 2> /tmp/namedPipe2 & 
                             NUSR1="$( cat /tmp/namedPipe2  )" 
                             usNM="${NUSR1,,}"
                             grep -q "^${usNM}:" /etc/passwd
                             
                             clear ; clear
                             passwd ${usNM}
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${usNM^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${usNM^} " 8 40
                                fi
                             # clean up
                             rm /tmp/namedPipe2 
                             ;; 
#----------------------------------------------------------------------
# Generate Password
                         4) 
                             clear ; clear
                             mkfifo /tmp/namedPipe10 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "6" 2> /tmp/namedPipe10 & 

                             # release contents of pipe
                             GNP1="$( cat /tmp/namedPipe10  )" 
                             clear
                             clear
                             HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "${GNP1}" )"

                             echo ""
                             echo "The Randomly Generated Password Is"
                             echo ""
                             echo "$HASH"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

                             dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Password Is \n\n$HASH " 10 40

                             # clean up
                             rm /tmp/namedPipe10
                             ;;

#--------------------------------------------------------------------------------------------------------------
# List ALL USERS on this Computer
                         5) 
                             clear ; clear
                             echo "Here is a List of ALL USERS on this Computer"
                             echo ""
                             LAU1="$(l=$(grep "^UID_MIN" /etc/login.defs) && awk -F':' -v "limit=${l##UID_MIN}" '{ if ( $3 >= limit ) print $1}' /etc/passwd)"
                             LAU2="$(echo "$LAU1" | grep  -v "nobody" )"
                             echo "$LAU2"
                             echo ""
                             echo ""                             
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

                             ;;
#----------------------------------------------------------------
# Add New User


 
                         6) 
                             clear ; clear
                             mkfifo /tmp/namedPipe8 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a New USER Name\nFor this Computer." 6 40 2> /tmp/namedPipe8 & 
                             NUSRN1="$( cat /tmp/namedPipe8  )" 
                             usrNM="${NUSRN1,,}"
                             grep -q "^${usrNM}:" /etc/passwd

                                if [[ "$?" = "0" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${usrNM^^}\" Already Exist! ! \nBOW will now close ! ! " 8 40 ; exit "${?}"
                                fi
                             clear ; clear
                             useradd -m -g users -G adm,lp,audio,video -s /bin/bash $usrNM
                             passwd $usrNM

                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "${usrNM^} Has Been Added" 8 40
                             # clean up
                             rm /tmp/namedPipe8
                             ;;

#-------------------------------------------------------------------
# Remove User
                         7) 
                             clear ; clear
                             mkfifo /tmp/namedPipe3 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter The USER \nYou Want to Delete." 6 40 2> /tmp/namedPipe3 & 
                             NUSRN5="$( cat /tmp/namedPipe3  )" 
                             UsrnM="${NUSRN5,,}"
                             grep -q "^${UsrnM}:" /etc/passwd

                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${UsrnM^^}\" Does NOT Exist! ! \nBOW will now close ! ! " 8 40 ; clear ; clear ; exit "${?}"
                                fi
                             clear
                             clear

                             OPTIONS=(1 "Delete ${UsrnM^} only"
                                      2 "Delete ${UsrnM^} and all files")
                             
                             CHOICE=$(dialog --clear \
                                             --ok-label "Continue" \
                                             --cancel-label "Back" \
                                             --backtitle "$DWS1" \
                                             --title "$title" \
                                             --menu "$MENU" \
                                             10 40 6 \
                                             "${OPTIONS[@]}" \
                                             2>&1 >/dev/tty)
                             case $CHOICE in

                             1)
                               clear && clear && userdel $UsrnM
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Only ${UsrnM^} Has Been Removed \nTheir folders Have NOT" 8 30
                             ;;

                             2)
                               clear && clear && userdel -r -f $UsrnM
                               echo ""
                               echo "Press a key to continue"
                               read -rsn1
                               echo "Key pressed" ; sleep 1
                               dialog --title "${DWS1}" --no-lines --msgbox "${UsrnM^} And ALL Their Folders \nHave Been Removed" 8 30
                             # clean up
                             rm /tmp/namedPipe3
                             ;;
                   esac
                   
                         ;;

#-------------------------------------------------------------------
# Change HostName

                         8) 
                             clear ; clear
                             mkfifo /tmp/namedPipe11   # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a new hostname" 6 40 "$hstnm" 2> /tmp/namedPipe11 & 
                             HOST_NAME="$( cat /tmp/namedPipe11  )" 
                             HsTnM="${HOST_NAME}"

                             echo $HsTnM > /etc/hostname && clear && clear
                             echo "Host Name Has Been Changed To"
                             echo "$HsTnM "
                             echo ""
                             echo "A Reboot May Be Required For Changes To Take Affect."
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Host Name Has Been Changed To \n$HsTnM \n\nA Reboot Is Required \nFor Changes To Take Affect." 9 40
                             # clean up
                             rm /tmp/namedPipe11
                             ;;
#-------------------------------------------------------------------
# How To Use
                         9)
                             clear ; clear ; break 2
                             ;;

                         10)
                             clear ; clear ; break 1                            
                            ;;
                 esac
                 done

        ;;
            
###############################################################################
################################ Lotto Fun ####################################
###############################################################################
        4)

            clear ; clear
            mkfifo /tmp/namedPipe5 # this creates named pipe, aka fifo
            # to make sure the shell doesn't hang, we run redirection 
            # in background, because fifo waits for output to come out    
            dialog --title "${DWS1}" --cancel-label "Ten Numbers" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "8" 2> /tmp/namedPipe5 & 
            # release contents of pipe
            LTO2="$( cat /tmp/namedPipe5  )" 
            clear
            clear
            LTO="$( seq -w 99 | sort -R | head -"${LTO2}" | fmt | tr " " "-" )"

            echo ""
            echo "The Randomly Generated Lotto Number Is"
            echo ""
            echo "$LTO"
            echo ""
            echo "Press a key to continue"
            read -rsn1
            echo "Key pressed" ; sleep 1

            dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Lotto Number Is \n\n$LTO " 10 45
            # clean up
            rm /tmp/namedPipe5
            echo ""
            ;;

###############################################################################
############################### Arch Setup ####################################
###############################################################################
        5)
          clear
          clear
                          while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "Refresh Mirrorlist"
                          2 "$IAD"
                          3 "connect with wifi menu"
                          4 "Install NetworkManager"
                          5 "Install A Login Manager"
                          6 "EXIT BOW"
                          7 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$ASUI" \
                                 --menu "$MENU" \
                                 $ASUI_HEIGHT $ASUI_WIDTH $ASUI_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in



                         1)
                                       if [ -f /usr/bin/reflector ]
                                          then
                                               clear ; clear ; dialog --title "${DWS1}" --no-lines --yesno  "This will refresh your mirrorlist.  \nIt may take a while. Are you ready?" 8 40
                                              if [[ "$?" = "0" ]]
                                                 then
                                                     clear
                                                     clear
                                                     echo "Fetching List" ; sleep 1
                                                    reflector --verbose --latest 50 --sort rate --save /etc/pacman.d/mirrorlist && clear && clear
                                                   dialog --title "${DWS1}" --no-lines --msgbox 'The Mirrorlist\nhas Been Refreshed \nUpdates and downloads \nShould be faster now. ' 8 25
                                                 else echo ""
                                                   clear 
                                                   clear
                                              fi
                                          else 
                                              clear
                                              clear
                                              echo "ERROR! "
                                              echo ""
                                              echo "Reflector could not be installed. "
                                              echo "If you are using a Manjaro distribution, "
                                              echo "Reflector is not Available. "
                                              echo "Manjaro uses Their own repositories. "
                                              echo "The \"Refresh Mirrorlist\" option will not function. "
                                              echo ""
                                              echo "However everthing else should work fine."
                                              echo ""
                                              echo "Press a key to continue"
                                              read -rsn1
                                              echo "Key pressed" ; sleep 1
                                              clear
                                              clear
                                       fi  
                                                
                           ;;


###############################################################################
######################### Install Another Desktop #############################
###############################################################################
                 
                         2)
                                  while [[ "$?" = "0" ]] ; do
                                  clear ; clear
                                  OPTIONS=(1 "$kubu1"
                                           2 "$lubu1"
                                           3 "$xubu1"
                                           4 "$gnome1"
                                           5 "$MATE1" 
                                           6 "$I3_GAPS"
                                           7 "$OPB1"
                                           8 "$EXT2"
                                           9 "$HOM3"
                                           10 "$BTAS")
                 
                                  CHOICE=$(dialog --clear \
                                                  --ok-label "Continue" \
                                                  --cancel-label "Back" \
                                                  --no-lines \
                                                  --backtitle "$DWS1" \
                                                  --title "$IAD" \
                                                  --menu "$MENU" \
                                                  $IAD_HEIGHT $IAD_WIDTH $IAD_CHOICE_HEIGHT \
                                                  "${OPTIONS[@]}" \
                                                  2>&1 >/dev/tty)
                                  case $CHOICE in
                                          1) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --                 yesno "This Will $kubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm plasma kde-applications
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $kubu1 Was successful" 8 30
                                                 fi
                                              ;; 

                                          2) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $lubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm lxde-common lxsession openbox lxde 
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $lubu1 Was successful" 8 30
                                                 fi
                                              ;;

                 
                                          3) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $xubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm xfce4 xfce4-goodies
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $xubu1 Was successful" 8 30
                                                 fi
                                              ;;

                                          4) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $gnome1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm gnome gnome-extra
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $gnome1 Was successful" 8 30
                                                 fi
                                              ;;

                                          5)
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $MATE1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm mate mate-extra
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $MATE1 Was successful" 8 30
                                                 fi
                                              ;;
 

                                          6)
                                             clear 
                                             clear
                                             dialog --title "${DWS1}" --no-lines --yesno "This Will $I3_GAPS \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm rxvt-unicode dialog wpa_supplicant openssl i3-gaps
                                                      
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $I3_GAPS Was successful" 8 30
                                                 fi
                                             ;;


                                          7)
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $OPB1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm openbox
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $OPB1 Was successful" 8 30
                                                 fi
                                              ;;

                                          
                                          8)
                                              clear && clear && break 3
                                              ;;

                                          9)
                                              clear && clear && break 2
                                              ;;

                                          10)
                                              clear && clear && break 1
                                              ;;
                          
                           esac
                           done
                          ;;


###############################################################################
################################# wifi menu ###################################
###############################################################################


                         3)
                           clear ; clear ; dialog --title "${ASUI}" --no-lines --yesno  "Connect with wifi-meu? \nIf you already have \nNetworkManager installed \nIt might cause a conflict\nAre you sure?" 10 30
                                 if [[ "$?" = "0" ]]
                                    then
                                          clear
                                          clear
                                          wifi-menu
                                          ping -c5 google.com
                                   else
                                        clear
                                 fi
                           
                           ;;
                        
                        
                         4)                              
                           clear ; clear
                              dialog --title "${DWS1}" --no-lines --yesno "This Will Install NetworkManager \nWould you like to continue?" 6 50
                              if [[ "$?" = "1" ]]
                                 then 
                                     dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear                                    
                                 else 
                                     clear && pacman -Sy --noconfirm networkmanager network-manager-applet
                                     systemctl start NetworkManager
                                     systemctl enable NetworkManager
                                     dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'Network Manager has been installed\nA Reboot Is Needed.' 8 30 ;   clear ; clear
                                 
                             fi
                           ;;
             

                         5) 
                             clear ; clear
                             OPTIONS=(1 "Install LightDM"
                                      2 "Install GDM"
                                      3 "Install SDDM"
                                      4 "Install LXDM"
                                      5 "Install Slim"
                                      6 "EXIT BOW"
                                      7 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$ILIM" \
                                 --menu "$MENU" \
                                 $ILIM_HEIGHT $ILIM_WIDTH $ILIM_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in



                                      1)
                                         clear ; clear
                                         echo ""
                                         echo "Installing Lightdm"
                                         sleep 2
                                         pacman -S --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
                                         systemctl enable lightdm   
                                         clear ; clear
                                         echo " Display manager will start the next time the  "
                                         echo " computer is restarted. a reboot is needed. " ; sleep 3
                                         ;;

                                      2) 
                                         clear ; clear
                                         echo ""
                                         echo "Installing GDM"
                                         sleep 2
                                         pacman -S --noconfirm gdm
                                         systemctl enable gdm
                                         clear
                                         clear
                                         echo " Display manager will start the next time the computer "
                                         echo " is restarted. a reboot is needed. " ; sleep 3
                                         ;; 

                                      3) 
                                         clear ; clear
                                         echo ""
                                         echo "Installing SDDM"
                                         sleep 2
                                         pacman -S --noconfirm sddm
                                         systemctl enable sddm
                                         clear
                                         clear
                                         echo " Display manager will start the next time the computer "
                                         echo " is restarted. a reboot is needed. " ; sleep 3
                                         ;;

                                      4)
                                         clear ; clear
                                         echo ""
                                         echo "Installing LXDM"
                                         sleep 2
                                         pacman -S--noconfirm lxdm
                                         systemctl enable lxdm
                                         clear
                                         clear
                                         echo " Display manager will start the next time the computer "
                                         echo " is restarted. a reboot is needed. " ; sleep 3
                                         ;;
                                      
                                      5)
                                         clear ; clear
                                         echo ""
                                         echo "Installing Slim"
                                         sleep 2
                                         pacman -S--noconfirm slim
                                         systemctl enable lxdm
                                         clear
                                         clear
                                         echo " Display manager will start the next time the computer "
                                         echo " is restarted. a reboot is needed. " ; sleep 3
                                         ;;
                                        

                                      6)
                                          clear ; clear ; break 3
                                         ;;

                                      7)
                                           clear ; clear ; break 1
                                         ;;

                  esac
                             ;;

                         6)
                             clear ; clear ; break 2
                             ;;

                         7)
                             clear ; clear ; break 1                            
                            ;;
                 esac
                 done
          ;;





###############################################################################
############################### How To Use ####################################
###############################################################################
        6)
            clear ; clear
            dialog --title "How To Use" --no-lines --msgbox "This is the HOME window of the BOW Toolbox.\nHere you chose which BOW Tool you want to use.\n\n$UPO1: To update your Opperating System.\nThis is the most used Tool.\n\n$IAD: It's as the title implies.\n\n$SFM: To create/delete or ajust Swapfies.\n\n$GP1: Just like the title says, \nit Generates a random password. A very useful Tool.\n\n$LF1: Is a tool  I made for my own amusement.\nIt will generate random lottory numbers.\nJust enter the amount of numbers you want.\n\n$EXT1: Choose between exiting this App, Sleep,\nrebooting, or turning off the Computer." 25 60
            clear ; clear

            ;;

###############################################################################
############################### EXIT Options ##################################
###############################################################################
        7)
            clear ; clear

                 OPTIONS=(1 "$EXT3"
                          2 "$LOUT"
                          3 "$SSP"
                          4 "$RB1"
                          5 "$SD1")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "OK" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$title" \
                                 --menu "$MENU" \
                                 $EXIT_HEIGHT $EXIT_WIDTH $EXIT_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             exit "${?}"
                             ;; 

                         2)
                             clear ; clear
                             kill -9 -1 && break 3
                             ;;

                         3)
                             clear ; clear
                             systemctl suspend && break 3
                             ;;

                         4) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Reboot YOUR SYSTEM! \nAre you sure you want to Reboot?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then reboot
                                   else echo ""
                                 fi
                             ;;
      

                         5) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Turn Off YOUR SYSTEM! \nAre you sure you want to Power Off \nyour computer?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then shutdown -h now
                                   else echo ""
                                 fi
                             ;;
                 esac
            ;;
###############################################################################
############################### END OF SCRIPT #################################
###############################################################################
esac
done
clear
