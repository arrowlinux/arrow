#!/bin/bash


## zddme.sh


#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 18/Jan/2019 undergoing development to date.
#------------------------------------------------------------------------------
# zddme is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, visit
#
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
##############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       paypal.me/eliwal
#
#------------------------------------------------------------------------------


TODAY="$(date)"
   if [ -f /usr/bin/pacman  ]

    then
           if [ -f /usr/bin/zenity ]
              then clear
                   clear
                 echo " Zenity is already installed"
                 echo ""
              else pkexec pacman -Sy --noconfirm zenity
                   echo "Installing Zenity"
           fi

           if [ -f /usr/bin/lsscsi ]
              then clear
                   clear
                 echo " lsscsi is already installed"
                 echo ""
              else (
                    echo "0"
                    echo "# Installing Lsscsi" 
                    pkexec pacman -Sy --noconfirm lsscsi
                    echo "100"
                   ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                    clear
                    clear
           fi
   fi

   if [ -f /usr/bin/apt ]
     then   
           if [ -f /usr/bin/zenity ]
              then clear
                   clear
                 echo " Zenity is already installed"
                 echo ""
              else pkexec apt install -y zenity
                   echo "Installing Zenity"

           fi

           if [ -f /usr/bin/lsscsi ]
              then clear
                   clear
                 echo " Zenity is already installed"
                 echo ""
              else (
                    echo "0"
                    echo "# Installing Lsscsi" 
                    pkexec apt install -y lsscsi
                    echo "100"
                   ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                    clear
                    clear
           fi
   fi        
#------------------------------------------------------------------
# 

DWS1='DD Me'
title2='Write an ISO image to USB'

DD_a_USB="Write An ISO To USB"
SDDUSB="Start DD Process"
EX1T='EXIT DD Me'
EXT2='Back to Home Menu'
EXT3='Just Exit This App'

DDME='DD a USB'
title='Z-DDme'
EXT1='EXIT'
BACK_HOME='Back to Home Menu'     

WRN="/home/eli/icons/dialog-warning.svg"
#-----------------------------------------------------------------
# 

while true ; do


home="$(zenity --list --title="$title HOME" --height=180 --width=200 --cancel-label="Restart App" --text="Hello ${usnm^} Welcome To Z-DDme\nChoose The Tool You Need." --radiolist --column "Pick" --column "Description"  FALSE "$DDME" TRUE "$EXT1" )"

#------------------------------------------------------------------------------

              if [[ "$home" = "$DDME" ]]
                 then zenity --question --width=270 --title='$title' --text "WARNING!!! WARNING!!! WARNING!!! \n this CAN TRASH YOUR COMPUTER!!! CONTINUE AT YOUR OWN RISK!!! \nAre you sure you want to continue?"
                     if [[ "$?" = "0" ]]
                        
                        # this is the most important part of this script
                        # this is the part that is dangerous."
                        then ISO1="$(zenity --file-selection --title="Choose source ISO / file" )"
                      
                             TSIL=` lsblk -lno NAME,TYPE,SIZE,MOUNTPOINT | grep "disk" `
                             LTSIL=` lsscsi | grep -v " ATA\|Generic" `

                             zenity --info --width=450 --title="$title" --text "Below is a list of the available drives on your system: Find your USB and remember its sdX \n\n$TSIL \n\nthese are the USB's connected \n\n${LTSIL}" 
                             
                             lsblk -lno NAME,TYPE,SIZE | grep 'disk' | awk '{print "/dev/" $1 " " $2}' | sort -u > devices.txt
                             sed -i 's/\<disk\>//g' devices.txt
                             devices=` awk '{print "FALSE " $0}' devices.txt `


                             dev=$(zenity --list  --radiolist --height=400 --width=350 --title='$title' --text "Select the drive that you want to install the ISO to. \nWARNING!! Be Sure It Is Your USB!!!" --column Drive --column Info $devices)
                       
                             lsblk -lno NAME,TYPE,SIZE,MOUNTPOINT ${dev} > /tmp/dev.txt   
                             cat /tmp/dev.txt | grep "disk" > /tmp/dev2.txt
                             dddd=` cat "/tmp/dev2.txt" `

                             LONG="${ISO1}"
                             SHORT=` basename "$LONG" | sed 's/\(.*\)\..*/\1/' `

                             zenity --question --window-icon="$WRN" --width=400 --title="POINT OF NO RETURN!! WARNING!!" --text="You Have Chosen \n$SHORT \n\nTo be installed at \n/dev/${dddd} \n\n\And the full command will be \n\n dd if=$ISO1 of=$dev bs=4M \n\nWarning! Warning! Warning! \nTHIS IS THE POINT OF NO RETURN!! \nMake sure that ${dev} is Your USB Drive! \nIf you accidentally select your system drive \nThis WILL erase it, and destroy your system!!!! \n\nAre you Sure You Are Ready?" 
 
                                    if [[ "$?" = "0" ]]
                        
                        # this is the most important part of this script
                        # this is the part that is dangerous. it over writes drives!!!!!!"
                                       then (
                                             echo "0"
                                             echo "# Installing The ISO to the USB"
                                             #pkexec mkfs.ext4 "${dev}1"
                                             pkexec dd if=$ISO1 of=$dev bs=4M
                                             echo "100"
                                             ) | zenity --progress --pulsate --auto-close --width=250 --no-cancel --title="$title"
                                                       zenity --info --width=150 --text="your USB is DONE"; rm /tmp/dev.txt  && rm /tmp/dev2.txt
                                       else break 3
                                    fi
                       
               
                  
                          # sub exit, this exits code 0
                        else exit 0
                     fi


              fi


              # this is the main exit opion, and exits code 0
              if [[ "$home" = "$EXT1" ]]
                 then exit 0
              fi

          

done

