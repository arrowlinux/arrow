#!/bin/bash

xfconf-query -c xfce4-desktop -m 
# Start monitoring channel "xfce4-desktop":

set: /backdrop/screen0/monitor1/workspace0/last-image
set: /backdrop/screen0/monitor1/workspace0/last-image
set: /backdrop/screen0/monitor1/workspace0/last-image
set: /backdrop/screen0/monitor1/workspace0/last-image
set: /backdrop/screen0/monitor1/workspace0/last-image

xfconf-query  \
  --channel xfce4-desktop \
  --property /backdrop/screen0/monitor1/workspace0/last-image \
  --set /usr/share/backgrounds/WALLppr/night_sky_4k.jpg
