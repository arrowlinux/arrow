#!/bin/bash


### this is in testig
# use at your own risk

dialog --title "${DWS1}" --no-lines --yesno "This Will copy Arrow to the hard drive, Would you like to continue?" 7 40
       if [[ "$?" = "1" ]]
          then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear ; exit
          else clear ; clear ; echo "Copying Files.... " ; echo "" ; sleep 1
       fi
       
# this script is from the late Great Midfinger. We miss you. we will never forget.
       
rsync -aAXvP /* /mnt --exclude={/dev/*,/proc/*,/sys/*,/tmp/*,/run/*,/mnt/*,/media/*,/lost+found,/.gvfs}

cp -avT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz /mnt/boot/vmlinuz-linux

sed -i 's/Storage=volatile/#Storage=auto/' /mnt/etc/systemd/journald.conf

rm /mnt/etc/udev/rules.d/81-dhcpcd.rules
rm /mnt/root/{.automated_script.sh,.zlogin}
rm -r /mnt/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm /mnt/etc/systemd/scripts/choose-mirror
rm /mnt/etc/sudoers.d/g_wheel
rm /mnt/etc/mkinitcpio-archiso.conf
rm -r /mnt/etc/initcpio

genfstab /mnt >> /mnt/etc/fstab && cat /mnt/etc/fstab

arch-chroot /mnt /bin/bash

umount /mnt -R
dialog --title "Arrow Install" --no-lines --msgbox "Instalation Complete" 5 25 ; clear
