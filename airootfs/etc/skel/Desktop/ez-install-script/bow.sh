#!/bin/bash

#======================================|
#              WELCOME TO              |
#======================================|
#  _____    ______    __    __    __   |
# |     \  /  __  \  |  |  |  |  |  |  | Better
# | **  / |  /  \  | |  |  |  |  |  |  | Or
# |    |  |  |  |  | |  |  |  |  |  |  | Worse
# | **  \ |  \__/  | |   \/    \/   |  |
# |_____/  \______/   \_____/\_____/   |
#======================================|
# Built for Arch Linux
#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 14/Jan/2019 undergoing development to date.
#------------------------------------------------------------------------------
# BOW is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Or visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch, it was brought to life by  
# an overwhelming number of requests by the arrowlinux group.
#
# For contributions, code, ideas, and other help,
# I give my deepest thanks to: knowledgebase101, erw1030, 
# leon.p (From EzeeTalk), and the arrowlinux group.
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       paypal.me/eliwal
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
#
#------------------------------------------------------------------------------
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if Zenity is installed. If not, it will install it.
TODAY="$(date)"

if [[ $USER = "root"  ]]
 then
        clear
      if [ -f /usr/bin/dialog ]
           then clear
                clear
                 echo " Dialog is already installed"
                 echo ""
           else pacman -Sy --noconfirm dialog
                clear
                clear
      fi
  echo " ROOT check passed "
  echo ""
  echo " =================================================="
  echo " ========== $TODAY =========="
  else
         dialog --no-lines --msgbox "ROOT check FAILED!\n\nIn a terminal run this with sudo.\n\nlike this     sudo ./bow.sh\nOr this       sudo bash bow.sh" 10 40 #background-color: red
         dialog --no-lines --msgbox "BOW will now close ! ! " 6 30
         clear
         clear
       echo " ROOT check FAILED (must be ran with sudo) like this "
       echo ""
       echo " sudo ./bow.sh"
       echo ""
       echo " =================================================== "
        exit "${?}"
fi
##################################################################################
#=================================================================================
#=================================================================================
# if you have an issue with the variable below, (Line #98) then change it    #====
# Enter your User Name there.                                                #====
#   Like this: USR1='username'   or   USR1="username"                        #====
USR1="$(logname)"                                                            #====
#=================================================================================
#=================================================================================
##################################################################################

DWS1='BOW Workshop'
title='BOW toolbox'
#------------------------------------------------------------------------------
EXT1='EXIT Options'
EXT2='Exit BOW'
EXT3='Just Exit This App'
UPO1='Update Options'
UPA1='Easy Update '
UPA2='Easy Update +AUR (yaourt) '
IAD='Install another Desktop'
PPA1='PPA Management'

SFM='Swapfile Management'
S0N="Turn All Swap On"
S0FF="Turn All Swap Off"

CHP1='USERS and Passwords'
GP1='Generate a Password'
LF1='Lotto Fun'
HTU='How to use this Toolbox'
HTU2='How to use this Tool'
BACK_HOME='Back to Home Menu'  
#------------------------------------------------------------------------------
TTLA="${USR1^} Pick An Update Option"
URS1='Update Repository List'
LAUB1='List Available Updates'
LAUB2='List Available Updates +AUR'

AUS1='Apply Updates (Safe)'
AULS1='Apply Updates (Less Safe)'
UPG1='Update Bootloader (grub)'
#------------------------------------------------------------------------------
kubu1='Install KDE Desktop'
lubu1='Install Lxde Desktop'
xubu1='Install Xfce Desktop'
gnome1='install Gnome 3 Desktop'
MATE1='Install MATE Desktop'
OPB1='Install OPENBOX'
#------------------------------------------------------------------------------
LAIP1='List All Installed PPAs'
lypp1='List PPAs You Have Added'
rmpp1='Remove a PPA You Have Added'
install_txt='Choose an IDE or Text Editor'
GRBCMZR='Grub Customizer'
UKUU1='Ubuntu Kernel Upgrade Utility'
ltbp1='Laptop Battery Power'
LMT1='Laptop Mode Tools'
CR1='Google Chrome'
gep1='Google Earth Pro'
UM1='Ubuntu Make'
BRP='Boot Repair'
BB1='Xiphos Bible'
NSI1='Numix Square Icons'
KB1='Kubuntu Backports'
CM1='Conky Manager'
#------------------------------------------------------------------------------
ST2='Sublime text 2'
VSC1='Visual Studio Code (Microsoft)'
BLF1='Bluefish Editor'
CDL='CodeLite Editor'
ATM1='Atom Text Editor'
BACKUP="Back To $PPA1"
#------------------------------------------------------------------------------
MES1='Manually Enter Size'
DS1='Delete Swapfile'
LFS1='Look At fstab, This is Safe'
ASF1='Add swapfile to fstab,  Dangerous! '
#------------------------------------------------------------------------------
USR="Change Password For ${USR1^} "
RT1='Change Password For ROOT'
OTR1='Change Password For Other User'
WH0='List ALL USERS on this Computer'
ANU1='Add a NEW User to the Computer'
DEL1='DELETE / REMOVE User (Danger!)'
CHHN='Change Hostname'
HowTU3='How to use This Tool'
#------------------------------------------------------------------------------
PA1='Power Adminastration'
RB1='Reboot This Computer'
SD1='Shut Down This Computer'
#------------------------------------------------------------------------------
HOM3='Back To Home Menu'
hstnm="$(hostname)"
SSP='Suspend to RAM. Sleep'
HBRNT='Hibernate. Power Off'
LOUT='Log Out Of Desktop'
ASUI="Arch Setup/Install"

while true ; do



# home, update
HOME_HEIGHT=15
HOME_WIDTH=33
HOME_CHOICE_HEIGHT=8

UPDATE_HEIGHT=15
UPDATE_WIDTH=40
UPDATE_CHOICE_HEIGHT=8

IAD_HEIGHT=17
IAD_WIDTH=35
IAD_CHOICE_HEIGHT=12

PPA_HEIGHT=26
PPA_WIDTH=40
PPA_CHOICE_HEIGHT=24

PPA_HEIGHT2=16
PPA_WIDTH2=40
PPA_CHOICE_HEIGHT2=9

SWAP_HEIGHT=25
SWAP_WIDTH=40
SWAP_CHOICE_HEIGHT=25

USER_HEIGHT=18
USER_WIDTH=42
USER_CHOICE_HEIGHT=15

LOTTO_HEIGHT=20
LOTTO_WIDTH=40
LOTTO_CHOICE_HEIGHT=10

HTU_HEIGHT=20
HTU_WIDTH=40
HTU_CHOICE_HEIGHT=10

NEW_HEIGHT=20
NEW_WIDTH=40
NEW_CHOICE_HEIGHT=10

ASUI_HEIGHT=15
ASUI_WIDTH=35
ASUI_CHOICE_HEIGHT=15

EXIT_HEIGHT=12
EXIT_WIDTH=35
EXIT_CHOICE_HEIGHT=6

HEIGHT=20
HEIGHT2=10
HEIGHT3=22

WIDTH=40
WIDTH2=50

CHOICE_HEIGHT2=12
CHOICE_HEIGHT3=22
CHOICE_HEIGHT4=20
CHOICE_HEIGHT=10
CHOICE_HEIGHT7=3
MENU="Choose an option:"
MMNU="Main Menu"

OPTIONS=(1 "$UPO1"
         2 "$SFM"
         3 "$CHP1"
         4 "$LF1"
         5 "$ASUI"
         6 "$HTU" 
         7 "$EXT1")

CHOICE=$(dialog --clear \
                --ok-label "continue" \
                --no-cancel \
                --no-lines \
                --backtitle "$DWS1" \
                --title "$MMNU" \
                --menu "$MENU" \
                $HOME_HEIGHT $HOME_WIDTH $HOME_CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
###############################################################################
########################### Update Management #################################
###############################################################################

        1)
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$LAUB1"
                          2 "$LAUB2"
                          3 "$UPA1"
                          4 "$UPA2"
                          5 "$UPG1" 
                          6 "$EXT2"
                          7 "$HOM3")

                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$UPO1" \
                                 --menu "$MENU" \
                                 $UPDATE_HEIGHT $UPDATE_WIDTH $UPDATE_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                            clear; clear ; pacman -Sy --color always ; clear ; clear ; OUTP="$(sudo pacman -Qu | wc -l)" ; pacman -Qu --color always ; echo "" ; echo "${OUTP} Available Updates (Not Including AUR)"
                            echo ""
                            echo "Press a key to continue"
                            read -rsn1
                            echo "Key pressed" ; sleep 1
                             ;;

                         2)
                             if [ -f /usr/bin/yaourt ]
                                then clear
                                     clear
                                 echo " yaourt is installed"
                                 echo ""
                                else dialog --title "${DWS1}" --no-lines --yesno "This Will install yaourt. \nWould you like to continue?" 6 40
                                    if [[ "$?" = "1" ]]
                                        then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear ; break 1
                                        else cd ~
                                             sudo pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                             git clone https://aur.archlinux.org/package-query.git
                                             cd package-query/ && makepkg -si
                                             cd ..
                                             git clone https://aur.archlinux.org/yaourt.git
                                             cd yaourt/ && makepkg -si
                                             cd ..
                                             sudo rm -dR yaourt/ package-query/

                                    fi
                             fi
                             
                             yaourt -Sy --color ; clear ; clear ; OUTP="$(yaourt -Qau | wc -l)" ; yaourt -Qau --color ; echo "" ; echo "${OUTP} Available Updates"
                            echo ""
                            echo "Press a key to continue"
                            read -rsn1
                            echo "Key pressed" ; sleep 1
                             ;;

                         3) 

                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System\nWould you like to continue?" 6 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else
                                              clear; clear ; pacman -Sy --color always ; clear ; clear ; OUTP="$(sudo pacman -Qu | wc -l)" ; pacman -Qu --color always ; echo "" ; echo "${OUTP} Available Updates (Not Including AUR)"
                                              echo "" ; sleep 10
                                 #             echo "Press a key to continue"
                                  #            read -rsn1
                                   #           echo "Key pressed" ; sleep 1
                                              pacman -Syu --noconfirm  --color always && clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                fi
                             ;; 

                         4) 
                           if [ -f /usr/bin/yaourt ]
                                then clear
                                     clear
                                     echo " yaourt is installed"
                                     echo ""
                                else dialog --title "${DWS1}" --no-lines --yesno "This Will install yaourt. \nWould you like to continue?" 6 40
                                    if [[ "$?" = "1" ]]
                                        then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear ; break 1
                                        else cd ~
                                             sudo pacman -S --needed --noconfirm base-devel git wget yajl zenity
                                             git clone https://aur.archlinux.org/package-query.git
                                             cd package-query/ && makepkg -si
                                             cd ..
                                             git clone https://aur.archlinux.org/yaourt.git
                                             cd yaourt/ && makepkg -si
                                             cd ..
                                             sudo rm -dR yaourt/ package-query/

                                    fi
                             fi
                            

                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update Your System\nWould you like to continue?" 6 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else
                                              clear; clear ; yaourt -Sy --color ; clear ; clear ; OUTP="$(sudo yaourt -Qau | wc -l)" ; yaourt -Qau --color ; echo "" ; echo "${OUTP} Available Updates"
                                              echo "" ; sleep 10

                                              sudo -u $(logname) yaourt -Sau --noconfirm  --color && sleep 5 && dialog --title "${DWS1}" --no-lines --msgbox "Update Complete" 5 20
                                fi
                             ;;

                         5)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --yesno "This Will Update GRUB Usually Safe\nBut Sometimes Not, Would you like to continue?" 7 40
                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made! ' 6 30 ;   clear ; clear
                                   else clear ;echo "" ; echo "Updating GRUB Bootloader" ; echo ""
                                        grub-mkconfig -o /boot/grub/grub.cfg
                                        echo ""
                                        echo "Press a key to continue"
                                        read -rsn1
                                        echo "Key pressed" ; sleep 1
                                        clear && clear && dialog --title "${DWS1}" --no-lines --msgbox "GRUB Update Complete" 6 25
                                fi
                             ;; 

                         6)
                             clear && clear && break 2
                             ;;

                         7)
                             clear && clear && break 1
                             ;;
                 esac
                 done
            ;;


###############################################################################
############################## Swapfile Management ############################
###############################################################################

        2)
        
                 clear
                 clear  
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "choose size"
                          2 "2GB"
                          3 "4GB"
                          4 "6GB" 
                          5 "8GB"
                          6 "16GB"
                          7 "32GB"
                          8 "$S0N"
                          9 "$S0FF"
                          10 "Delete Swapfile"
                          11 "Look at fstab (Safe)"
                          12 "Let us add swapfile to fstab"
                          13 "Manually Edit fstab DANGEROUS!"
                          14 "EXIT BOW"
                          15 "$HOM3")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$SFM" \
                                 --menu "$MENU" \
                                 $SWAP_HEIGHT $SWAP_WIDTH $SWAP_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             mkfifo /tmp/namedPipe1 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "This is where you enter the \nsize of swapfile (in GB) you want\nNumbers only, like 8 or 16" 8 40 "4.5" 2> /tmp/namedPipe1 & 

                             # release contents of pipe
                             swF="$( cat /tmp/namedPipe1  )" 
                             clear ; clear
                             swapoff -a && rm -f /swapfile
                             fallocate -l ${swF}G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "${swF}GB Swapfile Has Been Added" 8 30
                             # clean up
                             rm /tmp/namedPipe1 
                             ;; 
#----------------------------------------------------------------------

                         2) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 2G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "2GB Swapfile Has Been Added" 8 30
                             ;;

                         3) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 4G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "4GB Swapfile Has Been Added" 8 30
                             ;;

                         4) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 6G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "6GB Swapfile Has Been Added" 8 30
                             ;;

                         5) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 8G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "8GB Swapfile Has Been Added" 8 30
                             ;;



                         6) 
                             clear ; clear
                             swapoff -a
                             fallocate -l 16G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "16GB Swapfile Has Been Added" 8 30
                             ;;



                         7)
                             clear ; clear
                             swapoff -a
                             fallocate -l 32G /swapfile
                             chmod 600 /swapfile
                             mkswap /swapfile
                             swapon -a
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "32GB Swapfile Has Been Added" 8 30
                             ;;

                         8)
                            clear
                            clear
                            swapon -a
                            dialog --title "${S0N}" --no-lines --msgbox "All Swap Has Been Turned On" 6 33
                            ;;
                         


                         9)
                            clear
                            clear
                            swapoff -a
                            dialog --title "${S0N}" --no-lines --msgbox "All Swap Has Been Turned Off" 6 33
                  
                            ;;



                         10)
                             clear ; clear
                             swapoff -a && rm -f /swapfile
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;

                         11)
                             clear ; clear
                             dialog --title "${DWS1}" --no-lines --msgbox "At the bottom of the following \nlist, you are looking for \n\n /swapfile none swap defaults 0 0" 10 50
                             echo ""
                             echo " Add this to the bottom of fstab."
                             echo " /swapfile none swap defaults 0 0"
                             echo ""
                             clear ; clear
                             cat /etc/fstab
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             ;;
                         12)
                             echo "/swapfile none swap defaults 0 0" >> /etc/fstab
#                             nano /etc/fstab
                             dialog --title "${DWS1}" --no-lines --msgbox "swapfile was added to fstab. check to make sure it is, Make sure there is only one \ninstance of it.(no duplicates)" 10 50
                             ;;

                         13)
                             dialog --title "${DWS1}" --timeout 5 --no-lines --yesno  "Warning Editing fstab can \nBREAK YOUR SYSTEM! \n Are you sure you want to proceed?" 10 50
                             nano /etc/fstab
                             ;;

                         14)
                             clear ; clear
                             break 2
                             ;;
                         15)
                             clear ; clear
                             break 1
                 esac
                 done

        ;;

###############################################################################
############################ USERS AND PASSWORDS ##############################
###############################################################################
        3)

                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$RT1"
                          2 "$USR"
                          3 "$OTR1"
                          4 "$GP1" 
                          5 "$WH0"
                          6 "$ANU1"
                          7 "$DEL1"
                          8 "$CHHN"
                          9 "$HowTU3"
                          10 "EXIT BOW"
                          11 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$CHP1" \
                                 --menu "$MENU" \
                                 $USER_HEIGHT $USER_WIDTH $USER_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
#--------------------------------------------
# Change ROOT password
                         1) 
                             clear ; clear
                             passwd
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ROOT " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ROOT " 8 40
                                fi

                             ;; 
#--------------------------------------------
# Change USER password
                         2) 
                             clear ; clear
                             passwd $USR1
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${USR1^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${USR1^} " 8 40
                                fi
   
                             ;;
#-----------------------------------------------------------------------
# Change other users passwors 
                         3) 
                             clear ; clear
                             mkfifo /tmp/namedPipe2 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "enter new user name" 6 30 "${USR1,,}" 2> /tmp/namedPipe2 & 
                             NUSR1="$( cat /tmp/namedPipe2  )" 
                             usNM="${NUSR1,,}"
                             grep -q "^${usNM}:" /etc/passwd
                             
                             clear ; clear
                             passwd ${usNM}
                                if [ "$?" = "0" ]
                                     then dialog --title "${DWS1}" --no-lines --msgbox "Change password for ${usNM^} " 8 30

                                     else echo ""
                                          echo "Press any key to continue"
                                          read -rsn1
                                          echo "Key pressed" ; sleep 1
                                          dialog --title "${DWS1}" --no-lines --msgbox "FAILED to Change password for ${usNM^} " 8 40
                                fi
                             # clean up
                             rm /tmp/namedPipe2 
                             ;; 
#----------------------------------------------------------------------
# Generate Password
                         4) 
                             clear ; clear
                             mkfifo /tmp/namedPipe10 # this creates named pipe, aka fifo

                             # to make sure the shell doesn't hang, we run redirection 
                             # in background, because fifo waits for output to come out    
                             dialog --title "${DWS1}" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "6" 2> /tmp/namedPipe10 & 

                             # release contents of pipe
                             GNP1="$( cat /tmp/namedPipe10  )" 
                             clear
                             clear
                             HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "${GNP1}" )"

                             echo ""
                             echo "The Randomly Generated Password Is"
                             echo ""
                             echo "$HASH"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

#                            NPW1='Randomly Generated Password'

#                            HASH="$(< /dev/urandom tr -dc A-Za-z0-9_ | head -c "$NMB")"
#                             echo -n $HASH # | zenity --text-info --editable --height=10 --width=300 --title="The New $NPW1 Is"

                             dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Password Is \n\n$HASH " 10 40

                             # clean up
                             rm /tmp/namedPipe10
                             ;;

#--------------------------------------------------------------------------------------------------------------
# List ALL USERS on this Computer
                         5) 
                             clear ; clear
                             echo "Here is a List of ALL USERS on this Computer"
                             echo ""
                             LAU1="$(l=$(grep "^UID_MIN" /etc/login.defs) && awk -F':' -v "limit=${l##UID_MIN}" '{ if ( $3 >= limit ) print $1}' /etc/passwd)"
                             echo "$LAU1" 
                             echo ""
                             echo "User \"nobody\" is a pseudo user. And represents "
                             echo "the user with the least permissions on the system."
                             echo "It can be ignored."
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1

                             ;;
#----------------------------------------------------------------
# Add New User


 
                         6) 
                             clear ; clear
                             mkfifo /tmp/namedPipe8 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a New USER Name\nFor this Computer." 6 40 2> /tmp/namedPipe8 & 
                             NUSRN1="$( cat /tmp/namedPipe8  )" 
                             usrNM="${NUSRN1,,}"
                             grep -q "^${usrNM}:" /etc/passwd
#                             NIPTA=$(zenity --entry --title="$title" --width=250 --text "Please enter a New USER Name\nFor this Computer." )
#                             nusr="${NIPTA,,}"
#                             grep -q "^${nusr}:" /etc/passwd
                                if [[ "$?" = "0" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${usrNM^^}\" Already Exist! ! \nBOW will now close ! ! " 8 40 ; exit "${?}"
                                fi
                             clear ; clear
                             useradd -m -g users -G adm,lp,audio,video -s /bin/bash $usrNM
                             passwd $usrNM

                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "${usrNM^} Has Been Added" 8 40
                             # clean up
                             rm /tmp/namedPipe8
                             ;;

#-------------------------------------------------------------------
# Remove User
                         7) 
                             clear ; clear
                             mkfifo /tmp/namedPipe3 # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter The USER \nYou Want to Delete." 6 40 2> /tmp/namedPipe3 & 
                             NUSRN5="$( cat /tmp/namedPipe3  )" 
                             UsrnM="${NUSRN5,,}"
                             grep -q "^${UsrnM}:" /etc/passwd

                                if [[ "$?" = "1" ]]
                                   then dialog --title "${DWS1}" --no-lines --msgbox "ERROR User \"${UsrnM^^}\" Does NOT Exist! ! \nBOW will now close ! ! " 8 40 ; clear ; clear ; exit "${?}"
                                fi
                             clear
                             clear

                             OPTIONS=(1 "Delete ${UsrnM^} only"
                                      2 "Delete ${UsrnM^} and all files")
                             
                             CHOICE=$(dialog --clear \
                                             --ok-label "Continue" \
                                             --cancel-label "Back" \
                                             --backtitle "$DWS1" \
                                             --title "$title" \
                                             --menu "$MENU" \
                                             10 40 6 \
                                             "${OPTIONS[@]}" \
                                             2>&1 >/dev/tty)
                             case $CHOICE in

                             1)
                               clear && clear && userdel $UsrnM
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Only ${UsrnM^} Has Been Removed \nTheir folders Have NOT" 6 30
                             ;;

                             2)
                               clear && clear && userdel -r -f $UsrnM
                               echo ""
                               echo "Press a key to continue"
                               read -rsn1
                               echo "Key pressed" ; sleep 1
                               dialog --title "${DWS1}" --no-lines --msgbox "${UsrnM^} And ALL Their Folders \nHave Been Removed" 6 30
                             # clean up
                             rm /tmp/namedPipe3
                             ;;
                   esac
                   
                         ;;

#-------------------------------------------------------------------
# Change HostName

                         8) 
                             clear ; clear
                             mkfifo /tmp/namedPipe11   # this creates named pipe, aka fifo
                             dialog --title "${DWS1}" --inputbox "Please enter a new hostname" 6 40 "$hstnm" 2> /tmp/namedPipe11 & 
                             HOST_NAME="$( cat /tmp/namedPipe11  )" 
                             HsTnM="${HOST_NAME}"

                             echo $HsTnM > /etc/hostname && clear && clear
                             echo "Host Name Has Been Changed To"
                             echo "$HsTnM "
                             echo ""
                             echo "A Reboot Is Required For Changes To Take Affect."
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "Host Name Has Been Changed To \n$HsTnM \n\nA Reboot Is Required \nFor Changes To Take Affect." 9 40
                             # clean up
                             rm /tmp/namedPipe11
                             ;;
#-------------------------------------------------------------------
# How To Use
                         9) 
                             clear ; clear
                             echo "$HowTU3 goes here"
                             echo ""
                             echo "Press a key to continue"
                             read -rsn1
                             echo "Key pressed" ; sleep 1
                             dialog --title "${DWS1}" --no-lines --msgbox "$HowTU3 will go here" 8 30
                             ;;
#-------------------------------------------------------------------
# break loop or go home
                         10)
                             clear ; clear ; break 2
                             ;;

                         11)
                             clear ; clear ; break 1                            
                            ;;
                 esac
                 done

        ;;
            
###############################################################################
################################ Lotto Fun ####################################
###############################################################################
        4)

            clear ; clear
            mkfifo /tmp/namedPipe5 # this creates named pipe, aka fifo
            # to make sure the shell doesn't hang, we run redirection 
            # in background, because fifo waits for output to come out    
            dialog --title "${DWS1}" --cancel-label "Ten Numbers" --inputbox "How Strong? 1-90 \nUse Numbers ONLY!" 8 40 "8" 2> /tmp/namedPipe5 & 
            # release contents of pipe
            LTO2="$( cat /tmp/namedPipe5  )" 
            clear
            clear
            LTO="$( seq -w 99 | sort -R | head -"${LTO2}" | fmt | tr " " "-" )"

            echo ""
            echo "The Randomly Generated Lotto Number Is"
            echo ""
            echo "$LTO"
            echo ""
            echo "Press a key to continue"
            read -rsn1
            echo "Key pressed" ; sleep 1

            dialog --title "${DWS1}" --no-lines --msgbox "The Randomly Generated Lotto Number Is \n\n$LTO " 10 45
            # clean up
            rm /tmp/namedPipe5
            echo ""
            ;;

###############################################################################
############################### Arch Setup ####################################
###############################################################################
        5)
          clear
          clear
                          while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "Refresh Mirrorlist"
                          2 "$IAD"
                          3 "connect with wifi menu"
                          4 "Install NetworkManager"
                          5 "EXIT BOW"
                          6 "Back To Home Menu")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$ASUI" \
                                 --menu "$MENU" \
                                 $ASUI_HEIGHT $ASUI_WIDTH $ASUI_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in



                         1)

                           clear ; clear ; dialog --title "${DWS1}" --no-lines --yesno  "This will refresh your mirrorlist.  \nIt may take a while. Are you ready?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then
                                    
                                          clear
                                          if [ -f /usr/bin/reflector ]
                                             then clear
                                                  clear
                                                  echo " Dialog is already installed"
                                                 echo ""
                                             else pacman -Sy --noconfirm reflector
                                                  clear
                                                  clear
                                          fi

                                       clear
                                       clear
                                       echo "Fetching List" ; sleep 1
                                       reflector --verbose --latest 50 --sort rate --save /etc/pacman.d/mirrorlist && clear && clear
                                       dialog --title "${DWS1}" --no-lines --msgbox 'The Mirrorlist\nhas Been Refreshed \nUpdates and downloads \nShould be faster now. ' 8 25
                                   else echo ""
                                        clear 
                                        clear
                                 fi
                           ;;


###############################################################################
######################### Install Another Desktop #############################
###############################################################################
                 
                         2)
                                  while [[ "$?" = "0" ]] ; do
                                  clear ; clear
                                  OPTIONS=(1 "$kubu1"
                                           2 "$lubu1"
                                           3 "$xubu1"
                                           4 "$gnome1"
                                           5 "$MATE1" 
                                           6 "$OPB1"
                                           7 "$EXT2"
                                           8 "$HOM3"
                                           9 "Back to Arch Setup")
                 
                                  CHOICE=$(dialog --clear \
                                                  --ok-label "Continue" \
                                                  --cancel-label "Back" \
                                                  --no-lines \
                                                  --backtitle "$DWS1" \
                                                  --title "$IAD" \
                                                  --menu "$MENU" \
                                                  $IAD_HEIGHT $IAD_WIDTH $IAD_CHOICE_HEIGHT \
                                                  "${OPTIONS[@]}" \
                                                  2>&1 >/dev/tty)
                                  case $CHOICE in
                                          1) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --                 yesno "This Will $kubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm plasma kde-applications
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $kubu1 Was successful" 8 30
                                                 fi
                                              ;; 

                                          2) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $lubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm lxde-common lxsession openbox lxde 
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $lubu1 Was successful" 8 30
                                                 fi
                                              ;;

                 
                                          3) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $xubu1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm xfce4 xfce4-goodies
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $xubu1 Was successful" 8 30
                                                 fi
                                              ;;

                                          4) 
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $gnome1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm gnome gnome-extra
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $gnome1 Was successful" 8 30
                                                 fi
                                              ;;

                                          5)
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $MATE1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm mate mate-extra
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $MATE1 Was successful" 8 30
                                                 fi
                                              ;;
 

                                          6)
                                              clear ; clear
                                              dialog --title "${DWS1}" --no-lines --yesno "This Will $OPB1 \nWould you like to continue?" 6 50
                                                 if [[ "$?" = "1" ]]
                                                    then dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear
                                                    else 
                                                         clear && pacman -Sy --noconfirm openbox
                                                         echo ""
                                                         echo "Press a key to continue"
                                                         read -rsn1
                                                         echo "Key pressed" ; sleep 1
                                                         dialog --title "${DWS1}" --no-lines --msgbox "operation $OPB1 Was successful" 8 30
                                                 fi
                                              ;;

                                          
                                          7)
                                              clear && clear && break 3
                                              ;;

                                          8)
                                              clear && clear && break 2
                                              ;;

                                          9)
                                              clear && clear && break 1
                                              ;;
                          
                           esac
                           done
                          ;;


###############################################################################
################################# wifi menu ###################################
###############################################################################


                         3)
                           clear ; clear ; dialog --title "${ASUI}" --no-lines --yesno  "Connect with wifi-meu? \nIf you already have \nNetworkManager installed \nIt might cause a conflict\nAre you sure?" 10 30
                                 if [[ "$?" = "0" ]]
                                    then
                                          clear
                                          clear
                                          wifi-menu
                                          ping -c5 google.com
                                   else
                                        clear
                                 fi
                           
                           ;;
                        
                        
                         4)                              
                           clear ; clear
                              dialog --title "${DWS1}" --no-lines --yesno "This Will Install NetworkManager \nWould you like to continue?" 6 50
                              if [[ "$?" = "1" ]]
                                 then 
                                     dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'You Canceled\nNo Changes Have Been Made!' 8 30 ;   clear ; clear                                    
                                 else 
                                     clear && pacman -Sy --noconfirm networkmanager network-manager-applet
                                     systemctl start NetworkManager
                                     systemctl enable NetworkManager
                                     dialog --title "${DWS1}" --timeout 5 --no-lines --msgbox 'Network Manager has been installed\nA Reboot Is Needed.' 8 30 ;   clear ; clear
                                 
                             fi
                           ;;
             

                         5)
                             clear ; clear ; break 2
                             ;;

                         6)
                             clear ; clear ; break 1                            
                            ;;
                 esac
                 done
          ;;





###############################################################################
############################### How To Use ####################################
###############################################################################
        6)
            clear ; clear
            dialog --title "How To Use" --no-lines --msgbox "This is the HOME window of the BOW Toolbox.\nHere you chose which BOW Tool you want to use.\n\n$UPO1: To update your Opperating System.\nThis is the most used Tool.\n\n$IAD: It's as the title implies.\n\n$PPA1: To mount the PPA and install the App(s).\n\n$SFM: To create/delete or ajust Swapfies.\n\n$GP1: Just like the title says, \nit Generates a random password. A very useful Tool.\n\n$LF1: Is a tool  I made for my own amusement.\nIt will generate random lottory numbers.\nJust enter the amount of numbers you want.\n\n$EXT1: Choose between exiting this App, Sleep,\nrebooting, or turning off the Computer." 25 60
            clear ; clear

            ;;

###############################################################################
############################### EXIT Options ##################################
###############################################################################
        7)
            clear ; clear

                 OPTIONS=(1 "$EXT3"
                          2 "$LOUT"
                          3 "$SSP"
                          4 "$RB1"
                          5 "$SD1")
                 
                 CHOICE=$(dialog --clear \
                                 --ok-label "OK" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$title" \
                                 --menu "$MENU" \
                                 $EXIT_HEIGHT $EXIT_WIDTH $EXIT_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 
                             clear ; clear
                             exit "${?}"
                             ;; 

                         2)
                             clear ; clear
                             kill -9 -1 && break 3
                             ;;

                         3)
                             clear ; clear
                             systemctl suspend && break 3
                             ;;

                         4) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Reboot YOUR SYSTEM! \nAre you sure you want to Reboot?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then reboot
                                   else echo ""
                                 fi
                             ;;
      

                         5) 
                             clear ; clear
                             dialog --title "${DWS1}" --timeout 10 --no-lines --yesno  "This will Turn Off YOUR SYSTEM! \nAre you sure you want to Power Off \nyour computer?" 8 40
                                 if [[ "$?" = "0" ]]
                                   then shutdown -h now
                                   else echo ""
                                 fi
                             ;;
                 esac
            ;;
###############################################################################
############################### END OF SCRIPT #################################
###############################################################################
esac
done
clear
