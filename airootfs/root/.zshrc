#######################################################
############# Arch ZSH configuration file #############
#######################################################

### Set/unset ZSH options
#########################
# setopt NOHUP
setopt NOTIFY
# setopt NO_FLOW_CONTROL
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
# setopt AUTO_LIST
# setopt AUTO_REMOVE_SLASH
# setopt AUTO_RESUME
unsetopt BG_NICE
setopt CORRECT
setopt EXTENDED_HISTORY
# setopt HASH_CMDS
setopt MENUCOMPLETE
setopt ALL_EXPORT

### Set/unset  shell options
############################
setopt   notify globdots correct pushdtohome cdablevars autolist
setopt   correctall autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent 
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
unsetopt bgnice autoparamslash

### Autoload zsh modules when they are referenced
#################################################
autoload -U history-search-end
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
#zmodload -ap zsh/mapfile mapfile
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

### Set variables
#################
PATH="/usr/local/bin:/usr/local/sbin/:$PATH"
HISTFILE=$HOME/.zhistory
HISTSIZE=100000
SAVEHIST=100000
HOSTNAME="`hostname`"
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
### Load colors
###############

autoload colors zsh/terminfo
if [[ "$terminfo[colors]" -ge 8 ]]; then
   colors
fi
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
   eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
   eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
   (( count = $count + 1 ))
done


    #####################################################
   #######################################################
  #########################################################
 ###########################################################
###################### I ADDED THIS ######################### 

#xhost local:$USER > /dev/null

#nMn="$(if [ $? = 0 ]; then echo 0; else echo $?; fi)"
### Set prompt/theming part 1 ### pt2 is at about line 296
PR_NO_COLOR="%{$terminfo[sgr0]%}" 
PS1="%(!.${PR_RED}%n.$PR_BLUE%n)%(?.. [%?] ):--> "

#PS1="%(!.${PR_RED}%n.$PR_BLUE%n):$nMn--> "
#PS1="%(!.${PR_RED}%n.$PR_BLUE%n)${?#0}--> "
##############################################

### set prompt/theming Part 2 ###  pt1 is at line 68 ###
autoload -Uz promptinit
promptinit
prompt bart green yellow




### Set prompt # the original
##############
# PR_NO_COLOR="%{$terminfo[sgr0]%}"
# PS1="[%(!.${PR_RED}%n.$PR_LIGHT_YELLOW%n)%(!.${PR_LIGHT_YELLOW}@.$PR_RED@)$PR_NO_COLOR%(!.${PR_LIGHT_RED}%U%m%u.${PR_LIGHT_GREEN}%U%m%u)$PR_NO_COLOR:%(!.${PR_RED}%2c.${PR_BLUE}%2c)$PR_NO_COLOR]%(?..[${PR_LIGHT_RED}%?$PR_NO_COLOR])%(!.${PR_LIGHT_RED}#.${PR_LIGHT_GREEN}$) "
# RPS1="$PR_LIGHT_YELLOW(%D{%m-%d %_I:%M %p})$PR_NO_COLOR"
# unsetopt ALL_EXPORT

export VISUAL="nano"

##############################################################
##############################################################
####################### Set alias ############################
##############################################################
##############################################################

#-------------------------------------------------------------
#-------------------------------------------------------------
# this prints your IP address in the terminal: good for rsync
# and other scripts. you can use this as a variable in the script
# by just putting this: YourVar=($(IPADD))
ipL=($(hostname -i))
for ip in "${ipL[@]}"
do
  IPADD="$ip"
done
  IPADD2="$(hostname -i)"

alias mipa="clear ; echo ; echo your IP address is $IPADD2 ; echo ;"
# or
alias myipa="clear ; echo ; echo your IP address is $IPADD2 ; echo ;"
# or
alias myip="clear ; echo ; echo your IP address is $IPADD ; echo ;"
# or
alias mip="clear ; echo ; echo your IP address is $IPADD ; echo ;"
#----------------------------------------------------------------
#----------------------------------------------------------------


alias ll="ls -la"
alias ls="ls --color=auto "
alias l="ls"
alias la="ls -a"
alias sf="screenfetch"
alias nf="neofetch"
alias C="clear"
alias c="clear"
alias ccl=" clear ; clear ; ls -a"
alias X="exit"
alias x="exit"
alias cls="clear"
alias off="shutdown -h now"
alias leaf="leafpad"
alias snan="sudo nano"
alias sgny="sudo geany"
alias swifi="sudo wifi-menu"
alias ..="cd .."
alias ...="cd ../cd .."
alias rb="reboot"
alias p5="ping -c5 google.com"
alias p3="ping -c3 google.com"
alias wf="wifi-menu"
alias rsnm="sudo systemctl restart NetworkManager"
alias gl="glances"
alias ii="sudo inxi -Fdflmopux"
alias mem="clear;sudo inxi -mx"
alias axsh="chmod +x *.sh"
alias rxsh="chmod -x *.sh"
alias mine="sudo chown -R $USER:$USER ."
alias slp="systemctl suspend"
alias rt="sudo -s"
alias root="sudo su"


alias new="clear ; clear ; source ~/.zshrc"
alias desk="cd ~/Desktop"
alias docu="cd ~/Documents"
alias publ="cd ~/Public"
alias musi="cd ~/Music"
alias down="cd ~/Downloads"
alias temp="cd ~/Templates"
alias vide="cd ~/Videos"
alias pict="cd ~/Pictures"



alias bow="/home/liveuser/Desktop/ez-install-script/bow.sh"




######ARCH LINUX/Manjaro/Antergos####### 
alias lst="pacman -Sy --color always && clear && checkupdates | wc -l && pacman -Qu --color always"
alias srch=" pacman -Ss --color always"
#alias find=" pacman -Fs --color always"
alias pac=" pacman"
alias pacs="pacman -S --noconfirm --color always"
alias inst="pacman -S --noconfirm --color always"
alias yrt="yaourt -S --noconfirm --color always"
alias upd="pacman -Sy --color always
                yaourt -Sya --color"
alias sup="pacman -Syu --noconfirm --color always
                yaourt -Syau --noconfirm --color"
alias upg="pacman -Syu --noconfirm --color always"
     #           yaourt -Syau --noconfirm --color"
alias grb="grub-mkconfig -o /boot/grub/grub.cfg"
alias chk="checkupdates | wc -l "
alias cq="pacman -Sy ; clear ; clear ;sudo pacman -Qu | wc -l"
alias see="/home/eli/Scripts/see.sh ;sudo yaourt -Qau --color"
alias qa="pacman -Fs --color always "
alias what="pacman -Si"

alias del="pacman -Rs"

##### autoremove for Arch 
alias arm="pacman -R $(pacman -Qdtq)"

#############################################################
######################## End of My AIASES ###################
#############################################################

### Bind keys
#############
autoload -U compinit
compinit
bindkey "^?" backward-delete-char
bindkey '^[OH' beginning-of-line
bindkey '^[OF' end-of-line
bindkey '^[[5~' up-line-or-history
bindkey '^[[6~' down-line-or-history
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^r" history-incremental-search-backward
bindkey ' ' magic-space    # also do history expansion on space
bindkey '^I' complete-word # complete on tab, leave expansion to _expand
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

# Completion Styles

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
    
# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# command for process lists, the local web server details and host completion
# on processes completion complete all user processes
zstyle ':completion:*:processes' command 'ps -au$USER'

## add colors to processes for kill completion
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

#zstyle ':completion:*:processes' command 'ps -o pid,s,nice,stime,args'
#zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
#
#NEW completion:
# 1. All /etc/hosts hostnames are in autocomplete
# 2. If you have a comment in /etc/hosts like #%foobar.domain,
#    then foobar.domain will show up in autocomplete!
zstyle ':completion:*' hosts $(awk '/^[^#]/ {print $2 $3" "$4" "$5}' /etc/hosts | grep -v ip6- && grep "^#%" /etc/hosts | awk -F% '{print $2}') 
# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
    '*?.old' '*?.pro'
# the same for old style completion
#fignore=(.o .c~ .old .pro)

# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm apache bin daemon games gdm halt ident junkbust lp mail mailnull \
        named news nfsnobody nobody nscd ntp operator pcap postgres radvd \
        rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs avahi-autoipd\
        avahi backup messagebus beagleindex debian-tor dhcp dnsmasq fetchmail\
        firebird gnats haldaemon hplip irc klog list man cupsys postfix\
        proxy syslog www-data mldonkey sys snort
# SSH Completion
zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show



    #####################################################
   #######################################################
  #########################################################
 ###########################################################
###################### I ADDED THIS #########################



# RPS1="$PR_LIGHT_YELLOW(%D{%m-%d %_I:%M %p})$PR_NO_COLOR"


### Source plugins
##################
# for Arch based systems
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# for Debien based systems
# source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


export PATH=$PATH:$HOME/.scripts
export EDITOR="nano"
export TERMINAL="terminator"
export BROWSER="firefox"
#export BROWSER="chromium"
