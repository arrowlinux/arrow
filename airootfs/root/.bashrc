#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'

export PS1="\[\033[38;5;12m\][\[$(tput sgr0)\]\[\033[38;5;10m\]\u\[$(tput sgr0)\]\[\033[38;5;12m\]@\[$(tput sgr0)\]\[\033[38;5;7m\]\h\[$(tput sgr0)\]\[\033[38;5;12m\]]\[$(tput sgr0)\]\[\033[38;5;15m\]: \[$(tput sgr0)\]\[\033[38;5;7m\]\w\[$(tput sgr0)\]\[\033[38;5;12m\]>\[$(tput sgr0)\]\[\033[38;5;10m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

[ -e "/etc/DIR_COLORS" ] && DIR_COLORS="/etc/DIR_COLORS"
[ -e "$HOME/.dircolors" ] && DIR_COLORS="$HOME/.dircolors"
[ -e "$DIR_COLORS" ] || DIR_COLORS=""
eval "`dircolors -b $DIR_COLORS`"



## I ADDED THIS ###
export VISUAL="nano"

### Set alias
#############

alias ll="ls -la"
alias ls="ls --color=auto "
alias l="ls"
alias la="ls -a"
alias sf="screenfetch"
alias c="clear"
alias x="exit"
alias cls="clear"
alias off="shutdown -h now"
alias rut="sudo su"
alias root="sudo su"
alias leaf="leafpad"
alias snan="sudo nano"
alias sgny="sudo geany"
alias swifi="sudo wifi-menu"
alias ..="cd .."
alias ...="cd ../cd .."
alias rbt="reboot"
alias p5="ping -c5 google.com"
alias p3="ping -c3 google.com"
alias wf="sudo wifi-menu"

######ARCH LINUX#######< 
alias pac="sudo pacman"
alias pacs="sudo pacman -S"
alias yrts="yaourt -S"
alias yrt="yaourt"
alias inst="sudo pacman -S"
alias upd="sudo pacman -Sy"
alias supd="sudo pacman -Sy"
alias sup="sudo pacman -Syu"
alias ssup="sudo pacman -Syu"
alias upg="sudo pacman -Syu"
alias supg="sudo pacman -Syu"
alias grb="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias arm="sudo pacman -R $(pacman -Qdtq)"
alias cow="sudo mount -o remount,size=1G /run/archiso/cowspace"
alias arm="sudo pacman -Rs"
